# -*- encoding: utf-8 -*-

from Joueur import Joueur
import random
import time
from helper import Helper as hlp

class IA(Joueur):
    def __init__(self,parent,nom,systemeorigine,couleur):
        Joueur.__init__(self,parent,nom,systemeorigine,couleur)
        self.contexte="galaxie"
        self.delaiaction=random.randrange(5,10)  # le delai est calcule pour chaque prochaine action en seconde
        self.derniereaction=time.time()

    # NOTE sur l'analyse de la situation
    #          on utilise le temps (time.time() retourne le nombre de secondes depuis 1970) pour le delai de 'cool down'
    #          la decision dependra du contexte (modes de la vue)
    #          aussi presentement - on s'occupe uniquement d'avoir un vaisseau et de deplacer ce vaisseau vers
    #          le systeme le plus proche non prealablement visite.
    def analysesituation(self):
        t=time.time()
        if self.derniereaction and t-self.derniereaction>self.delaiaction:
            if self.contexte=="galaxie":
                if len(self.vaisseauxinterstellaires)==0:
                    c=self.parent.parent.cadre+5
                    if c not in self.parent.actionsafaire.keys():
                        self.parent.actionsafaire[c]=[]
                    self.parent.actionsafaire[c].append([self.nom,"creervaisseau",self.systemeorigine])
                    print("AJOUTER VAISSEAU ",self.systemeorigine.x,self.systemeorigine.y)
                else:
                    vcle=list(self.vaisseauxinterstellaires.keys())
                    vcle.sort()
                    ovi = self.vaisseauxinterstellaires
                    for k in vcle:
                        sanscible=[]
                        if ovi[k].cible==None:
                            sanscible.append(ovi)
                        if sanscible:
                            vi=random.choice(sanscible)
                            systtemp=None
                            systdist=1000000000000
                            scle=list(self.parent.systemes.keys())
                            scle.sort()
                            sys = self.parent.systemes
                            for j in scle:
                                viclelist = list(vi.keys())
                                vicle = viclelist[0]
                                d=hlp.calcDistance(vi[vicle].x,vi[vicle].y,sys[j].x,sys[j].y)
                                print ("DISTANCE ",k,d)
                                if d<systdist and j not in self.systemesvisites:
                                    systdist=d
                                    systtemp=sys[j]
                            if systtemp:
                                vi[vicle].ciblerdestination(systtemp)
                                print("CIBLER ",systtemp,systtemp.x,systtemp.y)
                            else:
                                print("JE NE TROUVE PLUS DE CIBLE")

                self.derniereaction=t
                self.delaiaction=random.randrange(5,10)
                print("CIV:" ,self.nom,self.couleur, self.delaiaction)


    def analysesituation1(self):
        t=time.time()
        if self.derniereaction and t-self.derniereaction>self.delaiaction:
            if self.contexte=="galaxie":
                if len(self.vaisseauxinterstellaires)==0:
                    c=self.parent.parent.cadre+5
                    if c not in self.parent.actionsafaire.keys():
                        self.parent.actionsafaire[c]=[]
                    self.parent.actionsafaire[c].append([self.nom,"creervaisseau",self.systemeorigine])

                    ##self.parent.actionsafaire[c].append([self.nom,"creervaisseau",self.systemeorigine])
#=======
                    #self.parent.actionsafaire[c].append([self.nom,"creervaisseau",self.systemeorigine.id])
                    self.parent.actionsafaire[c].append([self.nom,"creerstation",self.systemeorigine.id])
                    self.parent.actionsafaire[c].append([self.nom,"creervaisseauscout",self.systemeorigine.id])
                    print("AJOUTER VAISSEAU ",self.systemeorigine.x,self.systemeorigine.y)
                else:
                    for i in self.vaisseauxinterstellaires:
                        sanscible=[]
                        if i.cible==None:
                            sanscible.append(i)
                        if sanscible:
                            vi=random.choice(sanscible)
                            systtemp=None
                            systdist=1000000000000
                            for j in self.parent.systemes:
                                d=hlp.calcDistance(vi.x,vi.y,j.x,j.y)
                                print ("DISTANCE ",i,d)
                                if d<systdist and j not in self.systemesvisites:
                                    systdist=d
                                    systtemp=j
                            if systtemp:
                                vi.ciblerdestination(systtemp)
                                print("CIBLER ",systtemp,systtemp.x,systtemp.y)
                            else:
                                print("JE NE TROUVE PLUS DE CIBLE")

                self.derniereaction=t
                self.delaiaction=random.randrange(5,10)
                print("CIV:" ,self.nom,self.couleur, self.delaiaction)
