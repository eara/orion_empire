# -*- encoding: utf-8 -*-

from Id import Id

class Mine():
    def __init__(self,parent,nom,systemeid,planeteid,x,y):
        self.id=Id.prochainid()
        self.parent=parent
        self.x=x
        self.y=y
        self.systemeid=systemeid
        self.planeteid=planeteid
        self.entrepot=0
