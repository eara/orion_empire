# -*- encoding: utf-8 -*-

from Id import Id
        
class Labo():
    def __init__(self,parent,nom,systemeid,planeteid,x,y):     
        self.id=Id.prochainid()
        self.parent=parent
        self.x=x
        self.y=y
        self.systemeid=systemeid
        self.planeteid=planeteid

        self.coupwood = 400
        self.coupmine = 250
        self.coupunite = 4
        self.dureconstruction = 20
        
    def bonifierCaracteristiques(self, unite1, unite2):       
        if unite1.health > unite2.health:
            vie = unite1.health
            
        if unite2.health > unite1.health:
            vie = unite2.health
            
        if unite1.forceattaque > unite2.forceattaque:
            force = unite1.forceattaque
            
        if unite2.forceattaque > unite1.forceattaque:
            force = unite2.forceattaque
            
        if unite1.range > unite2.range:
            portee = unite1.forceattaque
            
        if unite2.range > unite1.range:
            portee = unite2.forceattaque
        
        if unite1.vitesse > unite2.vitesse:
            speed = unite1.forceattaque
            
        if unite2.vitesse > unite1.vitesse:
            speed = unite2.forceattaque
            
        uniteResult = Unite(self.nom, self.systemeid)
        uniteResult.definirattributs(vie,speed,force,portee)