# -*- encoding: utf-8 -*-

import random
from Systeme import Systeme
from Pulsar import Pulsar
from Joueur import Joueur
from IA import IA

class Modele():
    def __init__(self,parent,joueurs,dd):
        self.parent=parent
        self.diametre,self.densitestellaire,qteIA=dd
        self.nbsystemes=int(self.diametre**2/self.densitestellaire)
        print(self.nbsystemes)
        self.ias=[]    # IA
        self.joueurs={}
        self.joueurscles=joueurs
        self.actionsafaire={}
        self.pulsars=[]
        self.systemes={}
        self.terrain=[]
        self.creersystemes(int(qteIA))  # nombre d'ias a ajouter


    def creersystemes(self,nbias):  # IA ajout du parametre du nombre d'ias a ajouter

        for i in range(self.nbsystemes):
            x=random.randrange(self.diametre*10)/10
            y=random.randrange(self.diametre*10)/10
            tempsys = Systeme(x,y)
            self.systemes[tempsys.id] = tempsys

        for i in range(20):
            x=random.randrange(self.diametre*10)/10
            y=random.randrange(self.diametre*10)/10
            self.pulsars.append(Pulsar(x,y))

        np=len(self.joueurscles) + nbias  # on ajoute le nombre d'ias
        planes=[]

        systemetemp = list(self.systemes.keys())
        systemetemp.sort()

        while np:
            p=random.choice(systemetemp)
            print("la valeur de p est:" + p)

            if p not in planes and len(self.systemes[p].planetes)>0:
                planes.append(p)
                systemetemp.remove(p)
                np-=1
        couleurs=["cyan","goldenrod","orangered","greenyellow",
                  "dodgerblue","yellow2","maroon1","chartreuse3",
                  "firebrick1","MediumOrchid2","DeepPink2","blue"]    # IA ajout de 3 couleurs

        for i in self.joueurscles:
            self.joueurs[i]=Joueur(self,i,self.systemes[planes.pop(0)],couleurs.pop(0))

        for i in range(nbias): # IA
            nomia="IA_"+str(i)
            self.joueurscles.append(nomia)
            ia=IA(self,nomia,self.systemes[planes.pop(0)],couleurs.pop(0))
            self.joueurs[nomia]=ia  #IA
            self.ias.append(ia)  #IA

    def creervaisseau(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creervaisseau",systeme])

    def creervaisseauscout(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creervaisseauscout",systeme])

    def creervaisseaubombardier(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creervaisseaubombardier",systeme])

    def creervaisseauattaque(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creervaisseauattaque",systeme])

    def creervaisseaucargo(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creervaisseaucargo",systeme])

    def creervaisseautransporteur(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creervaisseautransporteur",systeme])

    def creerstation(self,systeme):
        self.parent.actions.append([self.parent.monnom,"creerstation",systeme])
        #Il faut peut-etre rajouter quelquechose ici

    def prochaineaction(self,cadre):
        if cadre in self.actionsafaire:
            for i in self.actionsafaire[cadre]:
                print(i)
                self.joueurs[i[0]].actions[i[1]](i[2])
            del self.actionsafaire[cadre]

        for i in self.joueurscles:
            self.joueurs[i].prochaineaction()

        for i in self.ias:
            i.analysesituation1()

        for i in self.pulsars:
            i.evoluer()


    def prochaineaction1(self,cadre):
        if cadre in self.actionsafaire:
            for i in self.actionsafaire[cadre]:
                self.joueurs[i[0]].actions[i[1]](i[2])
            del self.actionsafaire[cadre]

        for i in self.joueurscles:
            self.joueurs[i].prochaineaction()

        for i in self.ias:
            i.analysesituation1()

        for i in self.pulsars:
            i.evoluer()

    def changerproprietaire(self,nom,couleur,syst):
        self.parent.changerproprietaire(nom,couleur,syst)
