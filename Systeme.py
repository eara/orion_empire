# -*- encoding: utf-8 -*-

from Id import Id
from Etoile import Etoile
import random
from Planete import Planete

class Systeme():
    def __init__(self,x,y):
        self.id=Id.prochainid()
        self.proprietaire="inconnu"
        self.visiteurs={}
        self.diametre=50 # UA unite astronomique = 150000000km
        self.x=x
        self.y=y
        self.etoile=Etoile(self,x,y)
        self.planetes={}
        self.planetesvisites=[]
        self.creerplanetes()

    def creerplanetes(self):
        systemeplanetaire=random.randrange(5) # 4 chance sur 5 d'avoir des planetes
        if systemeplanetaire:
            nbplanetes=random.randrange(4,8)+1
            for i in range(nbplanetes):
                type=random.choice(["roc","gaz","glace"])
                distsol=random.randrange(30,60)/10 #distance en unite astronomique 150000000km
                taille=random.randrange(4,8)/25 # en masse solaire
                angle=random.randrange(360)
                tempplan = Planete(self,type,distsol,taille,angle)
                self.planetes[tempplan.id] = tempplan
