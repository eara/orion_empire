# -*- encoding: utf-8 -*-

class Planet():
    def __init__(self, x, y, rayon, tag):

        # Position
        self.x = x
        self.y = y
        self.rayon = rayon
        self.size = (rayon * 2, rayon * 2)

        # Propriétés Graphique
        self.id = None
        #self.image = None
        #self.tag = tag

        # Propiété
        self.owner = ""
        self.tag = tag
        self.minerai = 0
        self.metaux = 0
        self.gas = 0
        self.politic = 0
        self.millitaire = 0
        self.techno = 0

        self.stats = {
            'propiétaires': "",
            'minerai' : 0,
            'meteaux' : 0,
            'gaz': 0,
            'polique': 0,
            'militaire': 0,
            'technologie': 0}
    #def getInfo(self):


        #print("x:", self.x, "y:", self.y, "Rayon:", self.rayon)
