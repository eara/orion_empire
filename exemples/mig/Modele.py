# -*- encoding: utf-8 -*-

import random
import math
from Planet import *
from Joueur import *
from Vaisseau import *

class Modele():
    def __init__(self):
        # Constantes
        self.PETITE = 5
        self.MOYENNE = 15
        self.GRANDE = 30

        # Generateur de tag
        self.idGenerator = 1

        # Liste
        self.listeJoueur = []
        self.listeElements = []
        self.listePlanets = []
        self.listeVaisseaux = []
        self.actionsFutures = {}

    def genereteID(self):
        tmp = "id_" + str(self.idGenerator)
        self.idGenerator += 1
        return tmp

    def createPlayerUniverse(self):
        for player in self.listeJoueur:
            # Planet mère
            self.nRandomPlanete((player,'mere'), 1)
            self.listePlanets[len(self.listePlanets) - 1].owner = player
            # Premier cargo
            #self.createUnit(self.listePlanets[len(self.listePlanets) - 1], ('selectable','vaisseau','cargo'))
            # Planets Ressource
            #self.nRandomPlanete(('metaux',), 3)
            #self.nRandomPlanete(('gaz',), 3)

    def inscrireJoueur(self, newJoueur):
        #Cette Fonction devrait appartenir au serveur
        # Séparer la vérification de doublon de la création.
        for player in self.listeJoueur:
            if player.pseudo == newJoueur.pseudo or player.couleur == newJoueur.couleur:
                return False
        self.listeJoueur.append( Joueur( pseudo, couleur) )
        return True

    def nRandomPlanete(self, tag, n):
        if n > 0:

            # Generer une planete a position random
            x = random.randint(80, 800)
            y = random.randint(80, 500)
            #x = random.randint(80, 1000)
            #y = random.randint(80, 720)
            if 'mere' in tag:
                rayon = 75
            else:
                rayon = random.randint(15, 50)
            nouvellePlanet = Planet(x, y, rayon, tag)

            # Vérifie la proximité avec les autres planets
            planetTropProche = False
            for planet in self.listePlanets:
                if math.hypot(planet.x - nouvellePlanet.x, planet.y - nouvellePlanet.y) <= (planet.rayon * 2) + (nouvellePlanet.rayon * 2):
                    planetTropProche = True
                    break

            # Récursivité
            if planetTropProche:
                self.nRandomPlanete(tag, n)
            else:
                # Create Proper Tags
                tag_id = self.genereteID()
                nouvellePlanet.id = tag_id
                nouvellePlanet.tag = (tag_id, 'selectable', 'planet') + nouvellePlanet.tag
                # Add planet to list
                self.listePlanets.append(nouvellePlanet)
                self.nRandomPlanete(tag, n - 1)

    def createUnit(self, planet, size, tag):
        #size = (75, 75)
        new = Vaisseau( planet.x + planet.rayon, planet.y, size, planet.owner)
        new.id = self.genereteID()
        new.tag = (new.id,) + tag
        self.listeVaisseaux.append( new )
