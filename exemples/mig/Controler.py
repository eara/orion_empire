# -*- encoding: utf-8 -*-

import socket
import Pyro4
import os, os.path
import sys
import copy
from subprocess import Popen
from Modele import *
from Vue import *
from Planet import *

class Controler():
    def __init__(self):
        # Class Callback
        self.classCallback ={
            'getObjetById': self.getObjetById,
            'getSelectedStats': self.getSelectedStats,
            'setPosition': self.setPosition,
            'getIpAddresse': self.getIpAddresse,
            'hebergerPartie': self.hebergerPartie,
            'joindrePartie': self.joindrePartie,
            'boucleattente': self.boucleattente,
            'appendAction': self.appendAction,
            'partieSolo': self.partieSolo
        }
        self.listeActions = {
            'setPosition': self.setPosition,
            'serverAppendModele': self.serverAppendModele
        }
        self.listebutton=[["Attaque",self.attaque],["Test2",self.test2],["Test3",self.test3],["Test4",self.test4]]

        # Valeur pour le contoleur / Serveur:
        self.joueurCourant = Joueur(pseudo = "Emilie-man", couleur = "Bleu")
        self.joueurCourant.ip = self.getIpAddresse()
        #self.joueurCourant.ip = self.getIpAddresse()
        #self.ip =2 ################self.getIpAddresse()
        self.serverPID = None
        self.serveur = None
        self.cadre = 0
        self.actions = []
        self.attendre = 0

        # Initialisation de partie
        self.modele = Modele()
        self.egoserveur = 0
        self.mutiljoueur = True

        # Creation de Joueurs
        #self.modele.createPlayer( "SuperMan", "blue" )

        # Dieu Créa l'univers en 7 Jours,
        # je l'ai créer en une ligne de code
        #self.modele.createPlayerUniverse()

        # Creation de la vue
        self.vue = Vue( self.listebutton, self.classCallback)
        self.vue.couleurJoueur = "blue"
        self.vue.createLayout(self.joueurCourant)
        #self.vue.creercadresplash(self.ip, self.modele.listeJoueur[0])


        # Print infos a propos des éléments
        for planet in self.modele.listePlanets:
            print("x:", planet.x, "y:", planet.y, "Rayon:", planet.rayon, "size:", planet.size,  "id:", planet.id, "tag:", planet.tag)
        for planet in self.modele.listeVaisseaux:
            print("x:", planet.x, "y:", planet.y, "size:", planet.size,  "id:", planet.id, "tag:", planet.tag)

        #print("Start Runner")
        #self.runner()

    def lancerpartie(self):
        self.vue.changecadre(self.vue.boardFrame)
        # Dieu Créa l'univers en 7 Jours,
        # je l'ai créer en une ligne de code
        self.modele.createPlayerUniverse()
        self.modele.createUnit(self.modele.listePlanets[0], (75, 75), ('selectable','vaisseau', 'cargo'))
        self.modele.createUnit(self.modele.listePlanets[1], (158, 105), ('selectable','vaisseau', 'attaque'))
        self.vue.printElement(self.modele.listePlanets)
        self.vue.printElement(self.modele.listeVaisseaux)
        if self.mutiljoueur:
            self.mutilplayerRunner()
        else:
            self.runner()

    def getIpAddresse(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # on cree un socket
        s.connect(("gmail.com",80))    # on envoie le ping
        monip=s.getsockname()[0] # on analyse la reponse qui contient l'IP en position 0
        s.close() # ferme le socket
        return monip

    def startServerProcess(self):
        if self.egoserveur == 0:
            ordinateur = "windows"
            if ordinateur == "windows":
                pid = Popen(["C:\\Python34\\Python.exe", "./orion_mini_serveur.py"],shell=1).pid # on lance l'application serveur
                self.egoserveur=1
            elif ordinateur == "linux":
                pid = Popen(["/usr/bin/python", "./orion_mini_serveur.py"], shell = 1).pid
                self.serverPID = pid
                print("pid:", self.serverPID)
                self.egoserveur=1
            #self.connectToServer()
            #self.egoserveur=1 # on note que c'est soi qui, ayant demarre le serveur, aura le privilege de lancer la simulation

    def connectToServer(self):
        if self.egoserveur:
            ipserveur = self.joueurCourant.ip
        else:
            ipserveur = self.vue.lobbyItems['destinationIP'].get()

        nom = self.joueurCourant.pseudo # noter notre nom
        if ipserveur and nom:
            ad = "PYRO:controleurServeur@"+ipserveur+":50003" # construire la chaine de connection
            print(ipserveur)
            self.serveur = Pyro4.core.Proxy(ad) # se connecter au serveur
            #if self.serveur:
                #print("connected")
                #print(self.joueurCourant.pseudo)
                #rep = self.serveur.inscrireclient(str(self.joueurCourant.pseudo))    # on averti le serveur de nous inscrire
                #random.seed(rep[2]) # rep2 == La valeur pour le seed random
                #self.vue.changecadre(self.vue.lobbyFrame)
                #self.boucleattente()
            #tester retour pour erreur de nom

    def isConnected(self):
        if self.serveur:
            if self.serveur.testPyro() == 42:
                return True
        return False

    def enterLobby(self):
        self.vue.changecadre(self.vue.lobbyFrame)
        if self.egoserveur:
            self.vue.btnlancerpartie.config(state = NORMAL)
        rep = self.serveur.inscrireclient( str(self.joueurCourant.pseudo) ) # on averti le serveur de nous inscrire
        random.seed(rep[2]) # rep2 == La valeur pour le seed random
        self.boucleattente()

    def hebergerPartie(self):
        self.startServerProcess()
        self.connectToServer()
        if self.isConnected():
            self.enterLobby()

    def joindrePartie(self):
        self.connectToServer()
        if self.isConnected():
            self.enterLobby()

    """
    def lancerPartieReseau(self):
        rep = self.serveur.lancerpartie(1080, 900, 3)
        if rep[0][0] == 1: # rep[0] == 1 si tout les joueurs sont uniques et que la partie n'est pas déja débuter
            random.seed(rep[2]) # rep2 == La valeur pour le seed random
            for j in rep[0][1]: # Ajouter les joueurs Trié
                print(j)
                self.modele.listeJoueur.append( Joueur(pseudo = j))
            self.lancerpartie()
    """

    def partieSolo(self):
        #self.vue.creeJeuMap()
        self.mutiljoueur = False
        if self.joueurCourant.isSet():
            self.modele.listeJoueur.append(self.joueurCourant)
            self.lancerpartie()
            #self.vue.root.update()
        else:
            print("Pseudo invalide!!")
        #self.vue.creeJeuMap()
        #self.vue.root.update()


    def getObjetById(self, liste): # dla marde
        modeleObjet = []
        for n in liste:
            for v in self.modele.listeVaisseaux:
                if v.id in self.vue.spaceCanvas.gettags(n):
                    modeleObjet.append(v)
            for p in self.modele.listePlanets:
                if p.id in self.vue.spaceCanvas.gettags(n):
                    modeleObjet.append(v)
        return modeleObjet

    def getSelectedStats(self):
        for p in self.modele.listePlanets:
            if p.id in self.vue.spaceCanvas.gettags(self.vue.selected):
                return list(p.stats.items())
        for v in self.modele.listeVaisseaux:
            if v.id in self.vue.spaceCanvas.gettags(self.vue.selected):
                return list(v.stats.items())

    def setPosition(self, parametres):
        # Traitement server
        newPos = parametres[0]
        selected = parametres[1]
        if self.serveur:
            self.serveur.faireaction([self.joueurCourant.pseudo, self.cadre, ["setPosition", parametres] ])
        # Traitement local
        for e in selected:
            print("e dans self.selected:", e)
            for v in self.modele.listeVaisseaux:
                print("v dans listeVaisseaux:", v.id)
                print("vaiseau ID:", v.id, "tag dans element:", self.vue.spaceCanvas.gettags(e))
                if v.id in self.vue.spaceCanvas.gettags(e) and v.owner in self.vue.spaceCanvas.gettags(e):
                    print("True")
                    v.targetx = newPos[0]
                    v.targety = newPos[1]

    def moving(self):
        for v in self.modele.listeVaisseaux:
            if v.needToMove():
                delta = v.deplacer()
                self.vue.spaceCanvas.move(self.vue.spaceCanvas.find_withtag(v.id), delta[0], delta[1])

    def selectionEventListener(self): # Dla marde
        self.vue.drawSelection()
        if len(self.vue.selected) == 1:
            tmp = self.getObjetById(self.vue.selected)
            self.vue.showSelectionInfo(tmp[0].stats.copy())


    def runner(self):
        self.vue.drawSelection()
        self.moving()
        self.vue.root.after(40, self.runner)

    def mutilplayerRunner(self):
        self.vue.drawSelection()
        rep = self.serverAppendModele()
        print(self.modele.actionsFutures, rep)
        if self.serverSyncro( rep[1] ):
            self.animer(self.cadre)
            self.moving()
            self.cadre += 1
        else:
            self.cadre -= 1
        print("cadre: ", self.cadre)
        self.vue.root.after(40, self.mutilplayerRunner)

    def serverSyncro(self, message):
        if message == "attend":
            return False
        return True

    def serverAppendModele(self):
        # Fonction qui écoute la réponse du serveur et
        # ajuste le modèle en conséquence
        if self.actions:
            rep = self.serveur.faireaction([self.joueurCourant.pseudo, self.cadre, self.actions])
            self.actions = []
        else:
            rep = self.serveur.faireaction([self.joueurCourant.pseudo, self.cadre, 0])
        print(rep)
        if rep[0]: # si il y a des actions
            # pour toutes les cadres et les actions futur, les ajouters à la liste
            for cadre in rep[2]:
                if cadre not in self.modele.actionsFutures.keys(): # si le cadre n'est pas dans les actions futures
                    self.modele.actionsFutures[cadre] = [] #faire une entree dans le dictonnaire
                for action in rep[2][cadre]: # pour toutes les actions lies a une cle du dictionnaire d'actions recu
                    self.modele.actionsFutures[cadre].append(action) # ajouter cet action au dictionnaire sous l'entree dont la cle correspond a i
        return rep

    def animer(self, cadre):
        print("faire des actions")
        if cadre in self.modele.actionsFutures.keys():
            for parametres in self.modele.actionsFutures[cadre]:
                func = parametres[0]
                del parametres[0]
                self.listeActions[func](parametres)

    def boucleattente(self): # To ping the server
        rep = self.serveur.faireaction([self.joueurCourant.pseudo,0,0])
        print(rep[2])
        if rep[0]:
            print("Recu ORDRE de DEMARRER")
            for joueurs in list(rep[2][1][0][1]):
                self.modele.listeJoueur.append( joueurs )#rep[2][1][1] # rep[1][0][1] == listeDeJoueurTrier
            #self.cadre += 1
            self.lancerpartie()
        elif rep[0] == 0:
            self.vue.affichelisteparticipants(rep[2])
            self.vue.root.after(50,self.boucleattente)

    def appendAction(self, callback ,parametres):
        self.actions.append([self.joueurCourant.pseudo, callback, parametres])

    def fermefenetre(self):
        if self.serveur:
            self.serveur.quitter()
        #self.vue.root.destroy()

    #fonction de test qui sont appelees lorsqu'on pese sur un bouton
    def attaque(args):
        print("Attaque")
    def test2(args):
        print("test2")
    def test3(args):
        print("test3")
    def test4(args):
        print("test4")

if __name__ == '__main__':
    c = Controler()
    #c.runner()
    c.vue.root.mainloop()
    print(c.serverPID)
    #c.fermefenetre()
    #c.vue.closeImages()
