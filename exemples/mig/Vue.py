# -*- encoding: utf-8 -*-

from tkinter import *

from PIL import Image, ImageTk
from test.test_finalization import SelfCycleBase

class Vue():
    def __init__(self, listebutton, contolerCallback):
        # Dictionnaire des paths planet
        self.pngPlanets = {
            'neptune': 'planets/neptune.png',
            'earth': 'planets/earth.png',
            'jupiter': 'planets/jupiter.png',
            'mars': 'planets/mars.png'}
        self.pngVaisseaux = {
            'vaisseau': 'vaisseaux/deathStar.png',
            'attaque': 'vaisseaux/v2.png'
            }
        self.imagesBank = {
            'mere': None,
            'minerai': None,
            'metaux': None,
            'gaz': None,
            'cargo': None,
            'attaque': None
        }
        # Dictionnaire d'éléments
        self.images = []
        self.label = []
        self.lobbyItems = {}
        self.listebouton = listebutton
        self.selected = ()
        self.drag_data = {"x": None, "y": None, "item": None}
        self.couleurJoueur = None

        # Callback
        self.callBack = contolerCallback

        #self.ip=ip

        #dimension de la fenetre principale
        #self.largeur=900
        #self.hauteur=650

        # Root Window
        self.root = Tk()
        self.root.title("Orion")
        self.att = self.root.attributes()

        # Frames
        self.cadreactif = None
        self.cadreapp = Frame(self.root)
        self.cadreapp.pack()

        #self.boardFrame.pack(side = LEFT)

        #les canevas
        """
        self.spaceCanvas = None
        self.canevaslobby = None
        self.zone1 = None
        self.zone2 = None
        self.zone3 = None
        """
        # Mettre dans la liste des boutton
        #self.btnlancerpartie=Non

        #self.spaceCanvas.tag_bind(('mesTagAction',), '<Button-2>', self.onObjetRightClick)
        """
        #creation des differentes interfaces de jeu
        self.creercadrelobby()
        self.creercadresplash(self.ip , "emilie") ######################ajouter nom
        self.creeJeuMap()
        self.cadreactif=None
        #self.changecadre(self.cadrespace)
        #self.changecadreetat(self.spaceCanvas)
        #self.sidePanelFrame.pack_forget()
        self.changecadre(self.cadresplash)


    def creerpartie(self):
        self.sidePanelFrame.pack()
        self.changecadreetat(self.cadresplash)
        #self.sidePanelFrame.pack_forget()
        self.zone1.grid(row = 0, sticky = N)
        self.zone2.grid(row = 1)
        self.changecadre(self.cadrelobby)
    """
    def connecterpartie(self):
        self.callBack['connectToServer']()

     ##revoir :)
    def lancerpartie(self):
        self.cadrelobby.forget()
        self.changecadre(self.spaceCanvas)
        self.creationlistebutton(self.listebutton)
        self.parent.lancerpartie()


    def creercadresplash(self, joueur):
        self.splashFrame = Frame(self.cadreapp)
        self.splashFrame.pack()
        self.splashCanvas = Canvas(self.splashFrame, width=640, height=480, bg="red")
        self.splashCanvas.pack()
        dico = {'pseudo': Entry(bg="pink")}
        self.lobbyItems.update(dico)
        self.lobbyItems['pseudo'].insert(0, joueur.pseudo)
        dico = {'destinationIP':Entry(bg="pink")}
        self.lobbyItems.update(dico)
        self.lobbyItems['destinationIP'].insert(0, joueur.ip)  #[:-3])
        dico = {'ownIP': Label(text = joueur.ip, bg="red", borderwidth=0, relief=RIDGE)}
        self.lobbyItems.update(dico)
        dico = {'btn_host': Button(text="Héberger", bg="pink", command=self.callBack['hebergerPartie'])}
        self.lobbyItems.update(dico)
        dico = {'btn_rejoindre': Button(text = "Rejoindre", bg = "pink", command = self.callBack['joindrePartie'])}
        self.lobbyItems.update(dico)
        dico ={'btn_partieSolo': Button(text="Partie solo",bg="pink",command=self.callBack['partieSolo'])}
        self.lobbyItems.update(dico)
        self.splashCanvas.create_window(200,150,window=self.lobbyItems['pseudo'],width=100,height=30)
        self.splashCanvas.create_window(200,200,window=self.lobbyItems['destinationIP'],width=100,height=30)
        self.splashCanvas.create_window(200,250,window=self.lobbyItems['ownIP'],width=100,height=30)
        self.splashCanvas.create_window(200,300,window=self.lobbyItems['btn_host'],width=100,height=30)
        self.splashCanvas.create_window(200,350,window=self.lobbyItems['btn_rejoindre'],width=100,height=30)
        self.splashCanvas.create_window(200,400,window=self.lobbyItems['btn_partieSolo'],width=100,height=30)
        self.splashCanvas.pack()
        #self.splashFrame.pack_forget()
        print(dico)
        print(self.lobbyItems)

    def creercadrelobby(self):
        self.lobbyFrame = Frame(self.cadreapp)
        self.lobbyFrame.pack
        self.canevaslobby = Canvas(self.lobbyFrame, width=640, height=480, bg="lightblue")
        self.canevaslobby.pack()
        self.listelobby = Listbox(bg="red",borderwidth=0,relief=FLAT)
        self.modeauto = Entry(bg = "lightgreen")
        self.modeauto.insert(0, 0)
        self.nbetoile = Entry(bg = "pink")
        self.nbetoile.insert(0, 100)
        self.largeurespace = Entry(bg = "pink")
        self.largeurespace.insert(0, 750)
        self.hauteurespace = Entry(bg = "pink")
        self.hauteurespace.insert(0, 420)
        self.btnlancerpartie = Button(text="Lancer partie", bg="pink", command=self.callBack['boucleattente'], state = DISABLED)
        self.canevaslobby.create_window(440,240,window=self.listelobby,width=200,height=400)
        self.canevaslobby.create_window(200,200,window=self.largeurespace,width=100,height=30)
        self.canevaslobby.create_window(200,250,window=self.hauteurespace,width=100,height=30)
        self.canevaslobby.create_window(200,300,window=self.nbetoile,width=100,height=30)
        self.canevaslobby.create_window(100,300,window=self.modeauto,width=100,height=30)
        self.canevaslobby.create_window(200,400,window=self.btnlancerpartie,width=100,height=30)
        self.canevaslobby.pack()

    #creation du canevas de l'espace
    def creeJeuMap(self):
        self.boardFrame = Frame(self.cadreapp) # self.cadreapp = Frame(self.root,width=self.largeur,height=self.hauteur)
        self.boardFrame.pack()
        self.spaceFrame = Frame(self.boardFrame)
        self.spaceFrame.pack(side = LEFT)
        self.spaceCanvas = Canvas(self.spaceFrame, width = 1080, height = 900, bg = "black")
        self.spaceCanvas.pack()
        self.sideFrame = Frame(self.boardFrame, width = 200, height = 900)
        self.sideFrame.pack(side = RIGHT)
        self.sidePanel1 = Frame(self.sideFrame)
        self.sidePanel1.pack()
        self.sidePanel2 = Frame(self.sideFrame)
        self.sidePanel2.pack()
        self.sidePanel3 = Frame(self.sideFrame)
        self.sidePanel3.pack()

        # Bindings
        self.spaceCanvas.bind('<Button-1>', self.onLeftClick)
        self.spaceCanvas.bind('<B1-Motion>', self.onDrag)
        self.spaceCanvas.bind('<ButtonRelease-1>', self.onRealease)
        self.spaceCanvas.bind( '<ButtonRelease-3>', self.onRightClik)
        self.spaceCanvas.pack()

        self.creationlistebutton(self.listebouton)
        self.boardFrame.pack_forget()

    def createLayout(self, joueur):
        self.creercadresplash(joueur)
        self.creercadrelobby()
        self.creeJeuMap()
        self.changecadre(self.splashFrame)


    def changecadre(self,cadre):
        if self.cadreactif:
            self.cadreactif.pack_forget()
        self.cadreactif = cadre
        self.cadreactif.pack()
        self.root.update()

    def creationlistebutton(self,listebutton):
        #creation des boutons avec l'association a sa fonction
        for i in listebutton:
            j=Button(self.sidePanel2,text=i[0], command=i[1])
            j.pack()

    def affichelisteparticipants(self,lj):
        self.listelobby.delete(0,END)
        for i in lj:
            self.listelobby.insert(END,i)

    def resizeImage(self, size, image):
        # Resize picture
        return ImageTk.PhotoImage( image.resize(size, Image.ANTIALIAS) )

    def printElement(self, listeElements):
        # imprime et redimentionne les images
        for p in listeElements:
            # Selection d'images selon type de planets
                #Prototype
            if 'metaux' in p.tag:
                ima = Image.open(self.pngPlanets['mars'])
            elif 'gaz' in p.tag:
                ima = Image.open(self.pngPlanets['jupiter'])
            elif 'x' in p.tag:
                ima = Image.open(self.pngPlanets['neptune'])
            elif 'mere' in p.tag:
                ima = Image.open(self.pngPlanets['earth'])
            elif 'cargo' in p.tag:
                ima = Image.open(self.pngVaisseaux['vaisseau'])
            elif 'attaque' in p.tag:
                ima = Image.open(self.pngVaisseaux['attaque'])
            # Resize picture
            self.images.append(self.resizeImage(p.size, ima))
            ima.close()

            # Creation de l'image avec tkinter

            self.spaceCanvas.create_image(p.x, p.y, image = self.images[len(self.images) - 1], tags = p.tag)

    def drawSelection(self):
        self.spaceCanvas.delete("marqueur")
        pad = 10
        for i in self.selected:
            bbox = self.spaceCanvas.bbox((i,))
            self.spaceCanvas.create_oval( bbox[0] - pad, bbox[1] - pad, bbox[2] + pad , bbox[3] + pad , dash=(25,25), outline = self.couleurJoueur, tags=("marqueur",))

    def unselect(self):
        self.selected = ()
        self.spaceCanvas.delete("marqueur")
        #self.zone1.grid_remove()
        for i in self.label:
            i.grid_remove()
        self.label = []
        #print("unselect")

    def onLeftClick(self, event):
        self.drag_data['item'] = self.spaceCanvas.gettags(CURRENT)
        self.drag_data['x'] = event.x
        self.drag_data['y'] = event.y
        print(self.drag_data['x'], self.drag_data['y'], event.x, event.y, self.drag_data['item'])
        print(event.x, event.y)

    def onDrag(self, event):
        if ('selsqr',) == self.spaceCanvas.gettags("selsqr"):
            self.spaceCanvas.delete("selsqr")
        #print(self.drag_data['x'], self.drag_data['y'], event.x, event.y)
        self.spaceCanvas.create_rectangle(self.drag_data['x'], self.drag_data['y'], event.x, event.y, fill = "blue", stipple = "gray12", tags = ('selsqr',))
        self.spaceCanvas.update()
        #print(self.spaceCanvas.gettags("selsqr"))

    def onRealease(self, event):
        # Tout déselectionner avant de refaire une nouvelle selection
        self.unselect()
        self.spaceCanvas.delete("selsqr") # Carré de sélection

        tup = self.spaceCanvas.find_overlapping(self.drag_data['x'], self.drag_data['y'], event.x, event.y)
        if len(tup) > 1:
            # Prendre seulement les vaisseau
            for element in tup:
                if 'vaisseau' in self.spaceCanvas.gettags(element):
                    self.selected += (element,)
        elif len(tup) == 1:
            self.selected = tup
            self.showSelectionInfo()

    def onRightClik(self, event):
        # Détermine sur quels éléments le joueur à cliquer
        tup = self.spaceCanvas.gettags(CURRENT)
        if 'planet' in tup: # Sur une plannet
            print('Planet')
        elif 'vaisseau' in tup: # sur un vesseau
            print('vaisseau')
        else:
            #self.c.setPosition((event.x, event.y), self.selected)
            self.callBack['appendAction']("setPosition" ,[(event.x, event.y), self.selected]) 
            #self.c.moving()

    def showSelectionInfo(self):
        i = 0
        listStat = self.callBack['getSelectedStats']()
        print(listStat)
        for info in listStat:
            self.label.append( Label(self.sidePanel1, text = info[0], width = 10, height = 3, anchor = W, bg ="white", fg ="black"))
            self.label[len(self.label) - 1].grid( row = i, column = 0, sticky=(N, S, E, W))
            self.label.append( Label(self.sidePanel1, text = info[1], width = 10, height = 3, relief = GROOVE, anchor = W, bg ="white", fg ="black"))
            self.label[len(self.label) - 1].grid( row = i, column = 1, sticky=(N, S, E, W))
            i += 1

        #for lable in self.label:
        #    lable.pack()

    """
    def closeImages(self):
        for i in self.images:
            i.close()
    """
