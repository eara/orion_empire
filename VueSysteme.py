# -*- encoding: utf-8 -*-

from Perspective import Perspective
from tkinter import *
import math
from helper import Helper as hlp
from PIL import Image,ImageDraw, ImageTk
import random

class VueSysteme(Perspective):
    def __init__(self,parent):
        Perspective.__init__(self,parent)
        self.modele=self.parent.modele
        self.planetes={}
        self.systeme=None
        self.maselection=None

        self.UA2pixel=100 # ainsi la terre serait a 100 pixels du soleil et Uranus a 19 Unites Astronomiques
        self.largeur=self.modele.diametre*self.UA2pixel
        self.hauteur=self.largeur

        self.canevas.config(scrollregion=(0,0,self.largeur,self.hauteur))

        self.btncreervaisseau=Button(self.cadreetataction,text="Creer Vaisseau",command=self.creervaisseau)
        self.btncreervaisseau.pack(fill="x")

        self.btncreerstation=Button(self.cadreetataction,text="Creer Station",command=self.creerstation)
        self.btncreerstation.pack(fill="x")
        self.btnvuesysteme=Button(self.cadreetataction,text="Voir Planete",command=self.voirplanete)
        self.btnvuesysteme.pack(fill="x",side=BOTTOM)
        self.btnvuesysteme=Button(self.cadreetataction,text="Voir Galaxie",command=self.voirgalaxie)
        self.btnvuesysteme.pack(fill="x",side=BOTTOM)

        self.lbselectecible=Label(self.cadreetatmsg,text="Choisir Cible",bg="darkgrey")
        self.lbselectecible.pack(fill="x")
        self.changecadreetat(self.cadreetataction)

        self.nomcadreplanete = Label(self.cadreetatactionplanete, text = "--RESSOURCES DE LA PLANÈTE--")
        self.nomcadreplanete.pack(fill="x")
        self.nom2cadreplanete = Label(self.cadreetatactionplanete, text = "--SELECTIONNÉ--")
        self.nom2cadreplanete.pack(fill="x")
        self.nom3cadreplanete = Label(self.cadreetatactionplanete, text = "--EXPLOITABLE | UTILISABLE--")
        self.nom3cadreplanete.pack(fill="x")
        #self.canevas.create_image(x,y,image=self.images["scout"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
        #self.canevas.create_image(0,0, image=self.parent.images["wood"])
        #Tkinter.Label(root, image=tkimage, text="Update User", compound=Tkinter.CENTER).pack() # Put it in the display window

        self.woodplanete=Label(self.cadreetatactionplanete,image=self.parent.images["wood"], text="WOOD : ", compound=TOP, bg="yellow")#, image=goldimage)
        self.woodplanete.pack(fill="x")

        self.metalplanete=Label(self.cadreetatactionplanete,image=self.parent.images["metal"], text="METAL : ", compound=TOP,bg="purple", fg="white")
        self.metalplanete.pack(fill="x")

        self.foodplanete=Label(self.cadreetatactionplanete,image=self.parent.images["food"], text="FOOD : ", compound=TOP,bg="pink")
        self.foodplanete.pack(fill="x")
        self.populationplanete=Label(self.cadreetatactionplanete,image=self.parent.images["population"], text="POPULATION :",compound=TOP, bg="white")

        self.populationplanete.pack(fill="x")

    def afficherdecor(self,i):
        self.creerimagefond()
        self.affichermodelestatique(i)
        
    def creerimagefond(self): #NOTE - au lieu de la creer a chaque fois on aurait pu utiliser une meme image de fond cree avec PIL
        imgfondpil = Image.open("images/bgsystem.jpg")
        imgfondpil = imgfondpil.resize((self.hauteur, self.largeur), Image.ANTIALIAS)
        self.images["fond"] = ImageTk.PhotoImage(imgfondpil)
        self.canevas.create_image(self.largeur/2,self.hauteur/2,image=self.images["fond"])
    
    def voirplanete(self):
        self.parent.voirplanete(self.maselection)
        #self.parent.voirplanete(self.selected)

    def voirgalaxie(self):
        self.parent.voirgalaxie()

    def initsysteme(self,i):
        self.systeme=i
        self.afficherdecor(i)

    def affichermodelestatique(self,i):
        xl=self.largeur/2
        yl=self.hauteur/2
        n=self.modele.systemes[i].etoile.taille*self.UA2pixel/2
        mini=2
        UAmini=4
        #self.canevas.create_oval(xl-n,yl-n,xl+n,yl+n,fill="yellow",dash=(1,2),width=4,outline="white",
        #                         tags=("soleil",i))
        #Creation du soleil importé , la vue systeme - Humza
        #genere une selection alors ete mis en comment
     #   self.canevas.create_image(3000,3000, image = self.parent.vuesyst["vuesyst"])
        self.canevas.create_image(xl-n, yl-n, image = self.parent.images["soleil"], tags= ("soleil", i))
        
        self.minimap.create_oval(100-mini,100-mini,100+mini,100+mini,fill="yellow")

        for p in self.modele.systemes[i].planetes:
            x,y=hlp.getAngledPoint(math.radians(self.modele.systemes[i].planetes[p].angle),self.modele.systemes[i].planetes[p].distance*self.UA2pixel,xl,yl)
            n=self.modele.systemes[i].planetes[p].taille*self.UA2pixel
            #self.canevas.create_oval(x-n,y-n,x+n,y+n,fill="red",tags=("planete", i, p, "inconnu"))
            
          #Importation et affichage des images des planètes - Humza      
            cles = list(self.parent.imagesplanetes.keys())
            pla = random.choice(cles)
            self.canevas.create_image(x-n,y-n,image= self.parent.imagesplanetes[pla],tags=("planete", i, p, "inconnu"))
            
            x,y=hlp.getAngledPoint(math.radians(self.modele.systemes[i].planetes[p].angle),self.modele.systemes[i].planetes[p].distance*UAmini,100,100)
            self.minimap.create_oval(x-mini,y-mini,x+mini,y+mini,fill="red",tags=())

        # NOTE Il y a un probleme ici je ne parviens pas a centrer l'objet convenablement comme dans la fonction 'identifierplanetemere'
        canl=int(self.canevas.cget("width"))/2
        canh=int(self.canevas.cget("height"))/2
        self.canevas.xview(MOVETO, ((self.largeur/2)-canl)/self.largeur)
        self.canevas.yview(MOVETO, ((self.hauteur/2)-canh)/self.hauteur)

    def affichermodelestatique1(self,i):
        xl=self.largeur/2
        yl=self.hauteur/2
        n=i.etoile.taille*self.UA2pixel/2
        mini=2
        UAmini=4
        self.canevas.create_oval(xl-n,yl-n,xl+n,yl+n,fill="yellow",dash=(1,2),width=4,outline="white",
                                 tags=("systeme",i.id,"etoile",str(n),))
        self.minimap.create_oval(100-mini,100-mini,100+mini,100+mini,fill="yellow")
        for p in i.planetes:
            x,y=hlp.getAngledPoint(math.radians(p.angle),p.distance*self.UA2pixel,xl,yl)
            n=p.taille*self.UA2pixel
            self.canevas.create_oval(x-n,y-n,x+n,y+n,fill="red",tags=(i.proprietaire,"planete",p.id,"inconnu",i.id,int(x),int(y)))
            x,y=hlp.getAngledPoint(math.radians(p.angle),p.distance*UAmini,100,100)
            self.minimap.create_oval(x-mini,y-mini,x+mini,y+mini,fill="red",tags=())

        # NOTE Il y a un probleme ici je ne parviens pas a centrer l'objet convenablement comme dans la fonction 'identifierplanetemere'
        canl=int(self.canevas.cget("width"))/2
        canh=int(self.canevas.cget("height"))/2
        self.canevas.xview(MOVETO, ((self.largeur/2)-canl)/self.largeur)
        self.canevas.yview(MOVETO, ((self.hauteur/2)-canh)/self.hauteur)

    def montresystemeselection(self):
        self.changecadreetat(self.cadreetataction)
        
    def creervaisseau(self):
        pass

    #Avons-nous besoin de ceci

    def creerstation(self):
        if self.maselection[1] == "planete":
            self.maselection[1].creerstation

    def creervaisseauscout(self):
        pass

    def creervaisseauattaque(self):
        pass

    def creervaisseaubombardier(self):
        pass

    def creervaisseaucargo(self):
        pass

    def creervaisseautransporteur(self):
        pass

    def afficherpartie(self,mod):
        if self.maselection!=None:
            #joueur=self.modele.joueurs[self.parent.nom]
            #print(str(self.maselection))
            if 'planete' in self.maselection:
                self.montreplaneteselection(self.cadreetatactionplanete)
                #for i in self.systeme.planetes:
                self.woodplanete.config(text = "WOOD : " + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].wood) + ' | ' + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].woodExp))
                self.metalplanete.config(text = "METAL : " + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].ressourcemine)+ ' | ' + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].mineExp))
                self.foodplanete.config(text = "FOOD : " + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].food)+ ' | ' + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].foodExp))
                self.populationplanete.config(text = "POPULATION : " + str(self.modele.systemes[self.maselection[1]].planetes[self.maselection[2]].population))
            else:
                self.montreplaneteselection(None)
                # Laurence
                #self.montreplaneteselection(None)

    def afficherpartie1(self,mod):
        if self.maselection!=None:
            #joueur=self.modele.joueurs[self.parent.nom]
            #print(str(self.maselection))
            if 'planete' in self.maselection:
                self.montreplaneteselection()
                #for i in self.systeme.planetes:
                self.woodplanete.config(text = "WOOD : " + str(self.modele.systemes[self.maselection[4]].planetes[self.maselection[2]].wood))
                self.metalplanete.config(text = "METAL : " + str(self.modele.systemes[self.maselection[4]].planetes[self.maselection[2]].ressourcemine))
                self.foodplanete.config(text = "FOOD : " + str(self.modele.systemes[self.maselection[4]].planetes[self.maselection[2]].food))
                self.populationplanete.config(text = "POPULATION : " + str(self.modele.systemes[self.maselection[4]].planetes[self.maselection[2]].population))
            else:
                self.montreplaneteselection()

    def changerproprietaire(self):
        pass

    def afficherselection(self):
        if self.maselection!=None:
            joueur=self.modele.joueurs[self.parent.nom]
            if 'planete' in self.maselection:
                for i in self.systeme.planetes:
                    if i.id == self.maselection[2]:
                        x=int(self.maselection[3])
                        y=int(self.maselection[4])
                        t=20
                        self.canevas.create_oval(x-t,y-t,x+t,y+t,dash=(2,2),
                                                 outline=joueur.couleur,
                                                 tags=("select","selecteur"))
    """
            elif self.maselection[1]=="vaisseauinterstellaire":
                for i in joueur.vaisseauxinterstellaires:
                    if i.id == self.maselection[2]:
                        x=i.x
                        y=i.y
                        t=20
                        e=1
                        self.canevas.create_rectangle((x*e)-t,(y*e)-t,(x*e)+t,(y*e)+t,dash=(2,2),
                                                      outline=joueur.couleur,
                                                      tags=("select","selecteur"))

    def cliquervue(self,evt):
        self.changecadreetat(None)

        t=self.canevas.gettags("current")
        print(t)
        if t and t[0]!="current":
            nom=t[0]
            idplanete=t[2]
            idsysteme=t[4]
            #self.maselection=[self.parent.nom,t[1],t[2],t[5],t[6],t[4]]  # prop, type, id; self.canevas.find_withtag(CURRENT)#[0]
            self.maselection=t[1] # prop, type, id; self.canevas.find_withtag(CURRENT)#[0]
            if t[1]=="vaisseauinterstellaire":
                x = self.canevas.coords(t[0])
                print("vaisseau",x)
                self.maselection=[self.parent.nom,t[1],t[2]]
                self.montrevaisseauxselection()
            elif t[1] == "planete" and t[3]=="inconnu":
                if self.maselection and self.maselection[1]=="vaisseauinterstellaire":
                    #x = self.canevas.coords(t[0])
                    x = (self.canevas.canvasx(evt.x),self.canevas.canvasy(evt.y))
                    print("vaisseau apres planete",x)
                    self.parent.parent.ciblerdestinationsysteme(self.maselection[2],t[2],x)
                else:
                    x = self.canevas.coords(t[0])
                    print("planete",x)
                    self.maselection=[self.parent.nom,t[1],t[2],t[5],t[6],t[4]]  # prop, type, id; self.canevas.find_withtag(CURRENT)#[0]
                    self.montreplaneteselection()
            #elif t[1] == "inconnu"

            # ici je veux envoyer un message comme quoi je visite cette planete
            # et me mettre en mode planete sur cette planete, d'une shot
            # ou est-ce que je fais selection seulement pour etre enteriner par un autre bouton

            #self.parent.parent.atterrirdestination(nom,idsysteme,idplanete)
        else:
            print("Region inconnue")
            x = (self.canevas.canvasx(evt.x),self.canevas.canvasy(evt.y))
            print("vide",x)
            self.maselection=None
            self.lbselectecible.pack_forget()
            self.canevas.delete("selecteur")

    """

    #def montreplaneteselection2(self):
        #self.changecadreetatplanete(self.cadreetatactionplanete)

    def montrevaisseauxselection(self):
        self.changecadreetat(self.cadreetatmsg)

    def montreplaneteselection1(self):
        self.changecadreetat(self.cadreetataction)
    ##--------matlau
    def montreplaneteselection(self, cadre): # Pour enlever les labels??
        self.changecadreetatplanete(cadre)
    ##--------matlau

    def afficherartefacts(self,joueurs):
        pass #print("ARTEFACTS de ",self.nom)

    def cliquerminimap(self,evt):
        x=evt.x
        y=evt.y
        xn=self.largeur/int(self.minimap.winfo_width())
        yn=self.hauteur/int(self.minimap.winfo_height())

        ee=self.canevas.winfo_width()
        ii=self.canevas.winfo_height()
        eex=int(ee)/self.largeur/2
        eey=int(ii)/self.hauteur/2

        self.canevas.xview(MOVETO, (x*xn/self.largeur)-eex)
        self.canevas.yview(MOVETO, (y*yn/self.hauteur)-eey)

    #--------------------Ajout de Kassem------------------------------
    """
    def afficherpartie(self,mod):
        self.canevas.delete("artefact")
        self.afficherselection()

        for i in mod.joueurscles:
            i=mod.joueurs[i]
            for j in i.vaisseauxinterstellaires:

                x = j.systemex
                y = j.systemey

                #anglerotation
                anglerotation =  int (math.degrees(j.angletrajet))
                if j.angletrajet < 0 :
                   anglerotation = int (math.degrees(2*math.pi + j.angletrajet))

                if anglerotation > 337.5 or anglerotation <= 22.5 :
                    anglerotation = 0
                elif anglerotation <= 67.5:
                    anglerotation = 315
                elif anglerotation <= 112.5 :
                    anglerotation = 270
                elif anglerotation <= 157.5 :
                    anglerotation = 225
                elif anglerotation <= 202.5 :
                    anglerotation = 180
                elif anglerotation <= 247.5 :
                    anglerotation = 135
                elif anglerotation <= 292.5 :
                    anglerotation = 90
                elif anglerotation <= 337.5 :
                    anglerotation = 45

                if j.type == "scout":
                    self.canevas.create_image(x,y,image=self.parent.images["scout"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "attaque":
                    self.canevas.create_image(x,y,image=self.parent.images["attaque"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "bombardier":
                    self.canevas.create_image(x,y,image=self.parent.images["bombardier"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "cargo":
                    self.canevas.create_image(x,y,image=self.parent.images["cargo"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "transporteur":
                    self.canevas.create_image(x,y,image=self.parent.images["transporteur"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
        """
