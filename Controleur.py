# -*- encoding: utf-8 -*-

from Vue import Vue
import socket
from subprocess import Popen
import random
import Pyro4
from Modele import Modele

class Controleur():
    def __init__(self):
        print("IN CONTROLEUR")
        self.callBack = {'getModelObjet': self.getModelObjet,
                         'construire': self.construire}
        self.attente=0
        self.cadre=0 # le no de cadre pour assurer la syncronisation avec les autres participants
        self.egoserveur=0
        self.actions=[]    # la liste de mes actions a envoyer au serveur pour qu'il les redistribue a tous les participants
        self.monip=self.trouverIP() # la fonction pour retourner mon ip
        self.monnom=self.generernom() # un generateur de nom pour faciliter le deboggage (comme il genere un nom quasi aleatoire et on peut demarrer plusieurs 'participants' sur une meme machine pour tester)
        self.modele=None
        self.serveur=None
        self.vue=Vue(self,self.monip,self.monnom, self.callBack)
        self.vue.root.mainloop()

    def trouverIP(self): # fonction pour trouver le IP en 'pignant' gmail
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # on cree un socket
        s.connect(("gmail.com",80))    # on envoie le ping
        monip=s.getsockname()[0] # on analyse la reponse qui contient l'IP en position 0
        s.close() # ferme le socket
        return monip

    def generernom(self):  # generateur de nouveau nom - accelere l'entree de nom pour les tests - parfois a peut generer le meme nom mais c'est rare
        monnom="jmd_"+str(random.randrange(1000))
        return monnom

    def creerpartie(self):
        if self.egoserveur==0:
            pid = Popen(["C:\\Python34\\Python.exe", "./orion_empire_serveur.py"],shell=1).pid # on lance l'application serveur
            self.egoserveur=1 # on note que c'est soi qui, ayant demarre le serveur, aura le privilege de lancer la simulation

    ## ----------- FONCTION POUR CELUI QUI A CREE LA PARTIE SEULEMENT
    def lancerpartie(self,diametre,densitestellaire,qteIA): # reponse du bouton de lancement de simulation (pour celui qui a parti le serveur seulement)
        rep=self.serveur.lancerpartie(diametre,densitestellaire,qteIA)
    ## ----------- FIN --

    def inscrirejoueur(self):
        ipserveur=self.vue.ipsplash.get() # lire le IP dans le champ du layout
        nom=self.vue.nomsplash.get() # noter notre nom
        if ipserveur and nom:
            ad="PYRO:controleurServeur@"+ipserveur+":9999" # construire la chaine de connection
            self.serveur=Pyro4.core.Proxy(ad) # se connecter au serveur
            self.monnom=nom
            rep=self.serveur.inscrireclient(self.monnom)    # on averti le serveur de nous inscrire
            #tester retour pour erreur de nom
            random.seed(rep[2])

    def boucleattente(self):
        rep=self.serveur.faireaction([self.monnom,0,0])
        if rep[0]:
            print("Recu ORDRE de DEMARRER")
            self.initierpartie(rep[2])
        elif rep[0]==0:
            self.vue.affichelisteparticipants(rep[2])
            self.vue.root.after(50,self.boucleattente)

    def initierpartie(self,rep):  # initalisation locale de la simulation, creation du modele, generation des assets et suppression du layout de lobby
        if rep[1][0][0]=="lancerpartie":
            #print("REP",rep)
            self.modele=Modele(self,rep[1][0][1],rep[1][0][2]) # on cree le modele
            self.vue.afficherinitpartie(self.modele)
            #print(self.monnom,"LANCE PROCHAINTOUR")
            self.prochaintour()

    def prochaintour(self): # la boucle de jeu principale, qui sera appelle par la fonction bouclejeu du timer
        self.vue.modecourant.drawSelection()
        if self.serveur: # s'il existe un serveur
            self.cadre=self.cadre+1 # increment du compteur de cadre
            if self.actions: # si on a des actions a partager
                rep=self.serveur.faireaction([self.monnom,self.cadre,self.actions]) # on les envoie
            else:
                rep=self.serveur.faireaction([self.monnom,self.cadre,0]) # sinon on envoie rien au serveur on ne fait que le pigner
                                                                        # (HTTP requiert une requete du client pour envoyer une reponse)
            self.actions=[] # on s'assure que les actions a envoyer sont maintenant supprimer (on ne veut pas les envoyer 2 fois)
            if rep[1]=="attend":
                self.cadre=self.cadre-1 # increment du compteur de cadre
                print("J'attends")
            else:
                self.modele.prochaineaction(self.cadre)    # mise a jour du modele
                self.deplacement()
                self.vue.modecourant.afficherpartie(self.modele) # mise a jour de la vue
            if rep[0]: # si le premier element de reponse n'est pas vide
                for i in rep[2]:   # pour chaque action a faire (rep[2] est dictionnaire d'actions en provenance des participants
                                   # dont les cles sont les cadres durant lesquels ses actions devront etre effectuees
                    if i not in self.modele.actionsafaire.keys(): # si la cle i n'existe pas
                        self.modele.actionsafaire[i]=[] #faire une entree dans le dictonnaire
                    for k in rep[2][i]: # pour toutes les actions lies a une cle du dictionnaire d'actions recu
                        self.modele.actionsafaire[i].append(k) # ajouter cet action au dictionnaire sous l'entree dont la cle correspond a i
            self.vue.root.after(50,self.prochaintour)
        else:
            print("Aucun serveur connu")

    def prochaintour1(self): # la boucle de jeu principale, qui sera appelle par la fonction bouclejeu du timer
        if self.serveur: # s'il existe un serveur
            if self.attente==0:
                self.cadre=self.cadre+1 # increment du compteur de cadre
                self.modele.prochaineaction(self.cadre)    # mise a jour du modele
                self.vue.afficherpartie(self.modele) # mise a jour de la vue
            if self.actions: # si on a des actions a partager
                rep=self.serveur.faireaction([self.monnom,self.cadre,self.actions]) # on les envoie
            else:
                rep=self.serveur.faireaction([self.monnom,self.cadre,0]) # sinon on envoie rien au serveur on ne fait que le pigner
                                                                        # (HTTP requiert une requete du client pour envoyer une reponse)
            self.actions=[] # on s'assure que les actions a envoyer sont maintenant supprimer (on ne veut pas les envoyer 2 fois)
            if rep[0]: # si le premier element de reponse n'est pas vide
                for i in rep[2]:   # pour chaque action a faire (rep[2] est dictionnaire d'actions en provenance des participants
                                   # dont les cles sont les cadres durant lesquels ses actions devront etre effectuees
                    if i not in self.modele.actionsafaire.keys(): # si la cle i n'existe pas
                        self.modele.actionsafaire[i]=[] #faire une entree dans le dictonnaire
                    for k in rep[2][i]: # pour toutes les actions lies a une cle du dictionnaire d'actions recu
                        self.modele.actionsafaire[i].append(k) # ajouter cet action au dictionnaire sous l'entree dont la cle correspond a i
            if rep[1]=="attend": # si jamais rep[0] est vide MAIS que rep[1] == 'attend', on veut alors patienter
                if self.attente==0:
                    #self.cadre=self.cadre-1  # donc on revient au cadre initial
                    self.attente=1
                print("ALERTE EN ATTENTE",self.monnom,self.cadre)
            else:
                self.attente=0
                #print(self.cadre)
            self.vue.root.after(50,self.prochaintour)
        else:
            print("Aucun serveur connu")

    def fermefenetre(self):
        if self.serveur:
            self.serveur.jequitte(self.monnom)
        self.vue.root.destroy()

    # FONCTIONS DE COUP DU JOUEUR A ENVOYER AU SERVEUR
    # Methode de JMD
    """
    def creervaisseau(self,systeme):
        self.modele.creervaisseau(self.modele.systemes[systeme])
        #self.actions.append([self.monnom,"creervaisseau",""])
    """
    def creervaisseauscout(self,systeme):
        self.modele.creervaisseauscout(systeme)

    def creervaisseauattaque(self,systeme):
        self.modele.creervaisseauattaque(systeme)

    def creervaisseaubombardier(self,systeme):
        self.modele.creervaisseaubombardier(systeme)

    def creervaisseaucargo(self,systeme):
        self.modele.creervaisseaucargo(systeme)

    def creervaisseautransporteur(self,systeme):
        self.modele.creervaisseautransporteur(systeme)

    #Fonction creer par Kassem, fonction appele par la vue pour creer une station
    def creerstation(self,systeme):
        self.modele.creerstation(systeme)

    def ciblerdestination(self,idorigine,iddestination):
        self.actions.append([self.monnom,"ciblerdestination",[idorigine,iddestination]])

    def ciblerdestinationsysteme(self,idorigine,iddestination):
        self.actions.append([self.monnom,"ciblerdestinationsysteme",[idorigine,iddestination]])

    def visitersysteme(self,systeme_id):
        self.actions.append([self.monnom,"visitersysteme",[systeme_id]])

    def atterrirdestination(self,joueur,systeme,planete):
        self.actions.append([self.monnom,"atterrirplanete",[self.monnom,systeme,planete]])

    def creermine(self,joueur,systeme,planete,x,y):    
        #self.vue.affichermine(joueur,systeme,planete,x,y)
        if self.modele.systemes[systeme].planetes[planete].woodExp >=100 and self.modele.systemes[systeme].planetes[planete].mineExp >= 100:
            self.modele.systemes[systeme].planetes[planete].woodExp = self.modele.systemes[systeme].planetes[planete].woodExp -100
            self.modele.systemes[systeme].planetes[planete].mineExp = self.modele.systemes[systeme].planetes[planete].mineExp -100
            self.actions.append([self.monnom,"creermine",[self.monnom,systeme,planete, x,y]])
        
    def affichermine(self, joueur, systemeid, planeteid, mineId, x, y):
        self.vue.affichermine(joueur,systemeid,planeteid, mineId, x, y)
        
      #----------------------mathLau--------------------
    def creerMarcher(self,joueur,systeme,planete,x,y): 
        if self.modele.systemes[systeme].planetes[planete].woodExp >=200 and self.modele.systemes[systeme].planetes[planete].mineExp >= 200:
             self.modele.systemes[systeme].planetes[planete].woodExp = self.modele.systemes[systeme].planetes[planete].woodExp -200
             self.modele.systemes[systeme].planetes[planete].mineExp = self.modele.systemes[systeme].planetes[planete].mineExp -200
             self.actions.append([self.monnom,"creerMarcher",[self.monnom,systeme,planete, x,y]])  
       

    def affichermarcher(self,joueur,systemeid, planeteid, marcherId, x, y):
        self.vue.affichermarcher(joueur,systemeid,planeteid, marcherId, x, y)
        
#-------------mathLau---------------
    def creerbaraque(self,joueur,systeme,planete,x,y):
        if self.modele.systemes[systeme].planetes[planete].woodExp >=100 and self.modele.systemes[systeme].planetes[planete].mineExp >= 100:
            self.modele.systemes[systeme].planetes[planete].woodExp = self.modele.systemes[systeme].planetes[planete].woodExp -100
            self.modele.systemes[systeme].planetes[planete].mineExp = self.modele.systemes[systeme].planetes[planete].mineExp -100
            self.actions.append([self.monnom,"creerbaraque",[self.monnom,systeme,planete, x,y]])

    def afficherbaraque(self,joueur,systemeid,planeteid, baraqueId, x, y):
        self.vue.afficherbaraque(joueur,systemeid,planeteid, baraqueId, x, y)
        
    def creerferme(self,joueur,systeme,planete,x,y):
        if self.modele.systemes[systeme].planetes[planete].woodExp >=100 and self.modele.systemes[systeme].planetes[planete].mineExp >= 100 and self.modele.systemes[systeme].planetes[planete].population >=3:
            self.modele.systemes[systeme].planetes[planete].woodExp = self.modele.systemes[systeme].planetes[planete].woodExp -100
            self.modele.systemes[systeme].planetes[planete].mineExp = self.modele.systemes[systeme].planetes[planete].mineExp -100
            self.modele.systemes[systeme].planetes[planete].population = self.modele.systemes[systeme].planetes[planete].population - 3
            self.actions.append([self.monnom,"creerferme",[self.monnom,systeme,planete,x,y]])
        
    def afficherferme(self,joueur,systemeid,planeteid, fermeId, x, y):
        self.vue.afficherferme(joueur,systemeid,planeteid, fermeId, x, y)
   
    def creerunite(self,joueur,systeme,planete, x,y):
        if self.modele.systemes[systeme].planetes[planete].woodExp >=50 and self.modele.systemes[systeme].planetes[planete].mineExp >= 50:
            self.modele.systemes[systeme].planetes[planete].woodExp = self.modele.systemes[systeme].planetes[planete].woodExp -50
            self.modele.systemes[systeme].planetes[planete].mineExp = self.modele.systemes[systeme].planetes[planete].mineExp -50
            self.modele.systemes[systeme].planetes[planete].population = self.modele.systemes[systeme].planetes[planete].population + 1
            self.actions.append([self.monnom,"creerunite",[self.monnom,systeme,planete, x,y]])
            #self.vue.afficherunite(joueur,systeme,planete, uniteId, x, y)
    
    def afficherunite(self,joueur,systemeid,planeteid, uniteId, x, y):
        self.vue.afficherunite(joueur,systemeid,planeteid, uniteId, x, y)

    def creerlabo(self,joueur,systeme,planete,x,y):
        self.actions.append([self.monnom,"creerlabo",[self.monnom,systeme,planete,x,y]])

    def afficherlabo(self,joueur,systemeid,planeteid,laboId, x,y):
        self.vue.afficherlabo(joueur,systemeid,planeteid,laboId, x,y)
        
    def creerwood(self,joueur,systeme,planete,x,y):
        self.actions.append([self.monnom,"creerwood",[self.monnom,systeme,planete,x,y]])

    def afficherwood(self,joueur,systemeid,planeteid, woodId, x,y):
        self.vue.afficherwood(joueur,systemeid,planeteid, woodId, x,y)
        
    def creerchurch(self,joueur,systeme,planete,x,y):
        self.actions.append([self.monnom,"creerchurch",[self.monnom,systeme,planete,x,y]])

    def afficherchurch(self,joueur,systemeid,planeteid, churchId, x,y):
        self.vue.afficherchurch(joueur,systemeid,planeteid,churchId,x,y)

    def voirplanete(self,idsysteme,idplanete):
        pass

    def changerproprietaire(self,nom,couleur,systeme):
        self.vue.modes["galaxie"].changerproprietaire(nom,couleur,systeme)

    def getModelObjet(self):
        modeleObjet = []
        i = 0
        for element in self.vue.modecourant.selected:
            tag = self.vue.modecourant.canevas.gettags(element)
            if tag[0] == "systeme":
                 modeleObjet = self.modele.systemes[tag[1]]
            elif tag[0] == "planete":
                modeleObjet = self.modele.systemes[tag[1]].planetes[tag[2]]
            elif tag[0] == "unite":
                #c = self.modele.joueurs[tag[4]].uniteContruite.key()
                #print(self.modele.joueurs[tag[4]].uniteContruite.key())
                modeleObjet.append( self.modele.joueurs[tag[4]].uniteContruite[tag[3]])
                print(modeleObjet[i])
                i += 1
        
        return modeleObjet

    def construire(self, position):
        print(self.vue.modecourant.__class__.__name__)
        if self.vue.modecourant.__class__.__name__ == "VuePlanete":
            print("WORKING!!")
            
            commande = self.vue.contructObjet
            x = position[0]
            y = position[1]
            
            if commande == "mine":
                self.creermine(self.monnom,self.vue.modecourant.systeme, self.vue.modecourant.planete, x, y)
            
            elif commande == "baraque":
                self.creerbaraque(self.monnom,self.vue.modecourant.systeme,self.vue.modecourant.planete, x, y)
            
            elif commande == "ferme":
                self.creerferme(self.monnom,self.vue.modecourant.systeme,self.vue.modecourant.planete, x, y)
                
            elif commande == "unite":
                self.creerunite(self.monnom,self.vue.modecourant.systeme, self.vue.modecourant.planete, x, y)
            
            elif commande == "wood":
                self.creerwood(self.monnom,self.vue.modecourant.systeme, self.vue.modecourant.planete, x, y)
            
            elif commande == "labo":
                self.creerlabo(self.monnom, self.vue.modecourant.systeme, self.vue.modecourant.planete, x, y)
        
            elif commande == "church":
                self.creerchurch(self.monnom, self.vue.modecourant.systeme, self.vue.modecourant.planete, x, y)
                
            elif commande == "marcher":
                self.creerMarcher(self.monnom, self.vue.modecourant.systeme, self.vue.modecourant.planete,x,y)
                
    def construireunitedebase(self, x, y):
        if self.vue.modecourant.__class__.__name__ == "VuePlanete":
            commande = self.vue.contructObjet
            if commande == "uniteDeBase":
                self.creerunite(self.monnom,self.vue.modecourant.systeme, self.vue.modecourant.planete, x, y)
                
    def deplacement(self):
        touslesjoueurs = self.modele.joueurs.values()
        for joueurs in touslesjoueurs:
            touslesunites = joueurs.uniteContruite.values()
            for unite in touslesunites:
                print ("unite : x = " , unite.x , " newX = ", unite.newX, " y = ", unite.y, " newY = ", unite.newY )
                if unite.x != unite.newX or unite.y != unite.newY:
                    deltaX = self.calculdelta(unite.x, unite.newX, unite.vitesse)
                    deltaY = self.calculdelta(unite.y, unite.newY, unite.vitesse)
                    unite.x += deltaX 
                    unite.y += deltaY
                    self.vue.modecourant.animationDeplacement(unite.id, deltaX, deltaY)
                               
            touslesvaisseau = joueurs.vaisseauxinterstellaires.values()
            for vaisso in touslesvaisseau:
                if vaisso.x != vaisso.newX or vaisso.y != vaisso.newY:
                    vaisso.x = self.calculdelta(vaisso.x, vaisso.newX, vaisso.vitesse)
                    vaisso.y = self.calculdelta(vaisso.y, vaisso.newY, vaisso.vitesse)
                
    def calculdelta(self, pos1, pos2, vitesse):   
        if pos1 > pos2:
            if pos1 - vitesse < pos2:
                pos1 = pos2 - pos1
            else:
                pos1 = vitesse * -1
        elif pos1 < pos2:
            if pos1 + vitesse > pos2:
                pos1 = pos2 - pos1
            else:
                pos1 = vitesse

        return pos1
         #-------------mathLau--------------
