# -*- encoding: utf-8 -*-

from tkinter import *
import os,os.path
from os import startfile
import cv2
import numpy as np
from VueSysteme import VueSysteme
from VuePlanete import VuePlanete
from VueGalaxie import VueGalaxie
from PIL import Image,ImageDraw, ImageTk

class Vue():
    def __init__(self,parent,ip,nom,callBack,largeur=800,hauteur=600):
        self.root=Tk()
        self.root.title(os.path.basename(sys.argv[0]))
        self.root.protocol("WM_DELETE_WINDOW", self.fermerfenetre)
        self.parent=parent
        self.modele=None
        self.nom=None
        self.callBack = callBack
        self.largeur=largeur
        self.hauteur=hauteur
        self.images={}
        #cle pour les images planetes - Humza
        self.vuesyst={}
        self.imagesplanetes={}
        self.chargeimages()
        self.modes={}
        self.modecourant=None
        self.cadreactif=None
        self.creercadres(ip,nom)
        self.changecadre(self.cadresplash)
        self.contructObjet=None        
        self.bgImage
        self.bg_image_version
        self.bgImage2
        self.bg_image_version2

    def changemode(self,cadre):
        if self.modecourant:
            self.modecourant.pack_forget()
        self.modecourant=cadre
        self.modecourant.pack(expand=1,fill=BOTH)
        print(self.modecourant)

    def changecadre(self,cadre,etend=0):
        if self.cadreactif:
            self.cadreactif.pack_forget()
        self.cadreactif=cadre
        if etend:
            self.cadreactif.pack(expand=1,fill=BOTH)
        else:
            self.cadreactif.pack()

    def creercadres(self,ip,nom):
        self.creercadresplash(ip, nom)
        self.creercadrelobby()
        self.cadrejeu=Frame(self.root,bg="blue")
        self.modecourant=None

    def creercadresplash(self,ip,nom):
        self.cadresplash=Frame(self.root)
        self.canevasplash=Canvas(self.cadresplash,width=640,height=480,bg="red")

        ###############################
        #startfile('Y:/video-intro.mp4')
        cap = cv2.VideoCapture('./images/video-intro.mp4')
        try:
            while(cap.isOpened()):
                ret, frame = cap.read()

                gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

                cv2.imshow('frame',gray)
                if cv2.waitKey(25) & 0xFF == ord('q') & ret:
                    break
                
            cap.release()
            cv2.destroyAllWindows()
        except:
                cap.release()
                cv2.destroyAllWindows()
        ###############################
        self.bgImage = ImageTk.PhotoImage(Image.open('images/main_image.jpg'))
        self.bg_image_version = Label(self.canevasplash, image=self.bgImage)
        self.bg_image_version.place(x=0, y=0, relwidth=1, relheight=1)

        self.canevasplash.pack()
        self.nomsplash=Entry(bg="pink")
        self.nomsplash.insert(0, nom)
        self.ipsplash=Entry(bg="pink")
        self.ipsplash.insert(0, ip)
        labip=Label(text=ip,bg="white",borderwidth=0,relief=RIDGE)
        btncreerpartie=Button(text="Creer partie",bg="pink",command=self.creerpartie)
        btnconnecterpartie=Button(text="Connecter partie",bg="pink",command=self.connecterpartie)
        self.canevasplash.create_window(200,200,window=self.nomsplash,width=100,height=30)
        self.canevasplash.create_window(200,250,window=self.ipsplash,width=100,height=30)
        self.canevasplash.create_window(200,300,window=labip,width=100,height=30)
        self.canevasplash.create_window(200,350,window=btncreerpartie,width=100,height=30)
        self.canevasplash.create_window(200,400,window=btnconnecterpartie,width=100,height=30)

    def creercadrelobby(self):
        self.cadrelobby=Frame(self.root)
        self.canevaslobby=Canvas(self.cadrelobby,width=640,height=480,bg="lightblue")
        self.bgImage2 = ImageTk.PhotoImage(Image.open('images/second_image.jpg'))
        self.bg_image_version2 = Label(self.cadrelobby, image=self.bgImage2)
        self.bg_image_version2.place(x=0, y=0, relwidth=1, relheight=1)
        self.canevaslobby.pack()
        self.listelobby=Listbox(bg="red",borderwidth=0,relief=FLAT)
        self.diametre=Entry(bg="pink")
        self.diametre.insert(0, 12)
        self.densitestellaire=Entry(bg="pink")
        self.densitestellaire.insert(0, 2)
        self.qteIA=Entry(bg="pink")
        self.qteIA.insert(0, 0)
        self.btnlancerpartie=Button(text="Lancer partie",bg="pink",command=self.lancerpartie,state=DISABLED)
        self.canevaslobby.create_window(440,240,window=self.listelobby,width=200,height=400)
        self.canevaslobby.create_window(200,200,window=self.diametre,width=100,height=30)
        self.canevaslobby.create_text(20,200,text="Diametre en annee lumiere")

        self.canevaslobby.create_window(200,250,window=self.densitestellaire,width=100,height=30)
        self.canevaslobby.create_text(20,250,text="Nb systeme/AL cube")

        self.canevaslobby.create_window(200,300,window=self.qteIA,width=100,height=30)
        self.canevaslobby.create_text(20,300,text="Nb d'IA")

        self.canevaslobby.create_window(200,450,window=self.btnlancerpartie,width=100,height=30)

    def voirgalaxie(self):
        # A FAIRE comme pour voirsysteme et voirplanete, tester si on a deja la vuegalaxie
        #         sinon si on la cree en centrant la vue sur le systeme d'ou on vient
        s=self.modes["galaxie"]
        self.changemode(s)

    def voirsysteme(self, systeme = None):
        if systeme:
            if systeme in self.modes["systemes"].keys():
                s=self.modes["systemes"][systeme]
            else:
                s=VueSysteme(self)
                self.modes["systemes"][systeme]=s
                s.initsysteme(systeme)
            self.changemode(s)

    def voirsysteme1(self,systeme=None):
        if systeme:
            sid=systeme
            if sid in self.modes["systemes"].keys():
                s=self.modes["systemes"][sid]
            else:
                s=VueSysteme(self)
                self.modes["systemes"][sid]=s
                s.initsysteme(systeme)
            self.changemode(s)

    def voirplanete(self,maselection=None):
        s=self.modes["planetes"]
        if maselection:
            tag = self.modecourant.canevas.gettags(self.modecourant.selected)
            #print("QU'A T IL DANS LES TAG = ", tag)
            sysid=tag[1]
            planeid=tag[2]
            if planeid in self.modes["planetes"].keys():
                s=self.modes["planetes"][planeid]
            else:
                s=VuePlanete(self,sysid,planeid)
                self.modes["planetes"][planeid]=s
                s.initplanete(sysid,planeid)
            self.changemode(s)
        else:
            print("aucune planete selectionnee pour atterrissage")

    def voirplanete1(self,maselection=None):
        s=self.modes["planetes"]
        if maselection:
            sysid=maselection[4]
            planeid=maselection[2]
            if planeid in self.modes["planetes"].keys():
                s=self.modes["planetes"][planeid]
            else:
                s=VuePlanete(self,sysid,planeid)
                self.modes["planetes"][planeid]=s
                s.initplanete(sysid,planeid)
            self.changemode(s)
        else:
            print("aucune planete selectionnee pour atterrissage")

    def creerpartie(self):
        nom=self.nomsplash.get()
        ip=self.ipsplash.get()
        if nom and ip:
            self.parent.creerpartie()
            self.btnlancerpartie.config(state=NORMAL)
            self.connecterpartie()

    def connecterpartie(self):
        nom=self.nomsplash.get()
        ip=self.ipsplash.get()
        if nom and ip:
            self.parent.inscrirejoueur()
            self.changecadre(self.cadrelobby)
            self.parent.boucleattente()

    def lancerpartie(self):
        global modeauto
        diametre=self.diametre.get()
        densitestellaire=self.densitestellaire.get()
        qteIA=self.qteIA.get()  # IA
        if diametre :
            diametre=int(diametre)
        else:
            largeurjeu=None
        if densitestellaire :
            densitestellaire=int(densitestellaire)
        else:
            densitestellaire=None
        self.parent.lancerpartie(diametre,densitestellaire,qteIA)  #IA

    def affichelisteparticipants(self,lj):
        self.listelobby.delete(0,END)
        for i in lj:
            self.listelobby.insert(END,i)

    def afficherinitpartie(self,mod):
        self.nom=self.parent.monnom
        self.modele=mod

        self.modes["galaxie"]=VueGalaxie(self)
        self.modes["systemes"]={}
        self.modes["planetes"]={}

        g=self.modes["galaxie"]
        g.labid.config(text=self.nom)
        g.labid.config(fg=mod.joueurs[self.nom].couleur)

        #g.chargeimages()
        g.afficherdecor() #pourrait etre remplace par une image fait avec PIL -> moins d'objets
        self.changecadre(self.cadrejeu,1)
        self.changemode(self.modes["galaxie"])

    def affichermine(self,joueur,systemeid,planeteid, mineId, x, y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["mine"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im, tags = ("infrastructure", systemeid, planeteid, mineId))

    def afficherbaraque(self,joueur,systemeid,planeteid, baraqueId, x, y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["baraque"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, baraqueId))
                

    def afficherferme(self,joueur,systemeid,planeteid, fermeId, x, y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["ferme"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, fermeId))

    def affichermarcher(self,joueur,systemeid,planeteid, marcherId, x, y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["marcher"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, marcherId))

    def afficherunite(self,joueur,systemeid,planeteid, uniteId, x, y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["uniteT"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("unite", systemeid, planeteid, uniteId))


    def fermerfenetre(self):
        # Ici, on pourrait mettre des actions a faire avant de fermer (sauvegarder, avertir etc)
        self.parent.fermefenetre()
        self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid))
        
    ##------------------------------------------------matlau  
    
    def afficherlabo(self,joueur,systemeid,planeteid, laboId, x,y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["labo"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, laboId))
                
                
    def afficherwood(self,joueur,systemeid,planeteid, woodId, x,y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["wood"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, woodId))
                
    def afficherchurch(self,joueur,systemeid,planeteid, churchId, x,y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["church"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, churchId))
                
    def affichermarcher(self,joueur,systemeid,planeteid, marcherId, x,y):
        for i in self.modes["planetes"].keys():
            if i == planeteid:
                im=self.modes["planetes"][i].images["marcher"]
                self.modes["planetes"][i].canevas.create_image(x,y,image=im,tags = ("infrastructure", systemeid, planeteid, marcherId))
                
    def fermerfenetre(self):
        # Ici, on pourrait mettre des actions a faire avant de fermer (sauvegarder, avertir etc)
        self.parent.fermefenetre()
             ##------------------------------------------------matlau   
             
    def chargeimages(self):
        #code à JMD
        """
        im = Image.open("./images/chasseur.png")
        self.images["chasseur"] = ImageTk.PhotoImage(im)
        """

        #IMAGEACCEUIL
        im = Image.open("./images/ressources/population.png")
        im = im.resize((640,480))
        self.images["acceuil"] = ImageTk.PhotoImage(im)
        #IMAGE RESSOURCES
        im = Image.open("./images/ressources/food.png")
        im = im.resize((50,50))
        self.images["food"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/ressources/metal.png")
        im = im.resize((50,50))
        self.images["metal"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/ressources/wood.png")
        im = im.resize((50,50))
        self.images["wood"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/ressources/population.png")
        im = im.resize((50,50))
        self.images["population"] = ImageTk.PhotoImage(im)
     # --------------------mathLau----------------  
        im = Image.open("./images/batiments/marcher.png")
        im = im.resize((100,100))
        self.images["marcher"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/Unite/uniteTerrestre.png")
        im = im.resize((50,50))
        self.images["btnuniteT"] = ImageTk.PhotoImage(im)
        #Ajout de Humza et Kassem
        angle = 0
        # angle 0 = angle 90 en mathematique
        while angle < 360 :
        #Scout
            im = Image.open("./images/vaisseau_scout1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["scout"+str(angle)] = ImageTk.PhotoImage(im)
        #Attaque
            im = Image.open("./images/vaisseau_attaque1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["attaque"+str(angle)] = ImageTk.PhotoImage(im)
        #Bombardier
            im = Image.open("./images/vaisseau_bombardier1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["bombardier"+str(angle)] = ImageTk.PhotoImage(im)
        #Cargo
            im = Image.open("./images/vaisseau_cargo1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["cargo"+str(angle)] = ImageTk.PhotoImage(im)
        #Transporteur (meme image que le cargo)
            im = Image.open("./images/vaisseau_cargo1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["transporteur"+str(angle)] = ImageTk.PhotoImage(im)
            angle = angle + 45
        
        
        #Ajout images des planètes  dans un dictionnaire - Humza
           
        #Planete1
            im = Image.open("./images/planets/planete1.png")
            im = im.resize((80,70))
            self.imagesplanetes["planet1"] = ImageTk.PhotoImage(im)
            
        #Planete2
            im = Image.open("./images/planets/planete2.png")
            im = im.resize((70,70))
            self.imagesplanetes["planete2"] = ImageTk.PhotoImage(im)
            
        #Planete3
            im = Image.open("./images/planets/planete3.png")
            im = im.resize((70,70))
            self.imagesplanetes["planete3"] = ImageTk.PhotoImage(im)
        
        #Planete4
            im = Image.open("./images/planets/planete4.png")
            im = im.resize((70,70))
            self.imagesplanetes["planete4"] = ImageTk.PhotoImage(im)
            
        #Planete5
            im = Image.open("./images/planets/planete5.png")
            im = im.resize((70,70))
            self.imagesplanetes["planete5"] = ImageTk.PhotoImage(im)
        
        #Planete6
            im = Image.open("./images/planets/planete6.png")
            im = im.resize((70,70))
            self.imagesplanetes["planete6"] = ImageTk.PhotoImage(im)
        
        #Soleil
            
            im = Image.open("./images/planets/sun.png")
            im = im.resize((170,170))
            self.images["soleil"] = ImageTk.PhotoImage(im)
            
        #Vue Systeme
        
            im = Image.open("./images/planets/vuesyst.jpg")
            im = im.resize((1000,1000))
            self.vuesyst["vuesyst"] = ImageTk.PhotoImage(im)
        
        