# -*- encoding: utf-8 -*-

from Perspective import Perspective

from Unite import Unite

from tkinter import *
from PIL import Image,ImageDraw, ImageTk

class VuePlanete(Perspective):
    def __init__(self,parent,syste,plane):
        Perspective.__init__(self,parent)
        self.modele=self.parent.modele
        self.planete=plane
        self.systeme=syste
        self.infrastructures={}
        
        self.listparams=[]
        #self.maselection=None

        self.KM2pixel=100 # ainsi la terre serait a 100 pixels du soleil et Uranus a 19 Unites Astronomique
        self.largeur=self.modele.diametre*self.KM2pixel
        self.hauteur=self.largeur

        self.canevas.config(scrollregion=(0,0,self.largeur,self.hauteur))
        self.canevas.config(bg="steel blue")

        self.btncreervaisseau=Button(self.cadreetataction,text="Creer Mine",command=self.creermine)
        self.btncreervaisseau.pack(fill="x")

        self.btncreerbaraque=Button(self.cadreetataction,text="Creer Baraque",command=self.creerbaraque)
        self.btncreerbaraque.pack(fill="x")
        
        self.btncreerferme=Button(self.cadreetataction,text="Creer Bouffe",command=self.creerferme)
        self.btncreerferme.pack(fill="x")

        self.btncreerlabo=Button(self.cadreetataction,text="Creer Labo",command=self.creerlabo)
        self.btncreerlabo.pack(fill="x")
        
        self.btncreerwood=Button(self.cadreetataction,text="Creer Bois",command=self.creerwood)
        self.btncreerwood.pack(fill="x")
        
        self.btncreerchurch=Button(self.cadreetataction,text="Creer Religion",command=self.creerchurch)
        self.btncreerchurch.pack(fill="x")

        #-------------------mathLau----------------------
        self.btncreerMarcher=Button(self.cadreetataction,text="Creer Marcher",command=self.creerMarcher)
        self.btncreerMarcher.pack(fill="x")
        #--------------------mathLau-----------------------------------------
        
        #self.btncreerstation=Button(self.cadreetataction,text="Creer Manufacture",command=self.creermanufacture)
        #self.btncreerstation.pack(fill="x")

        self.changecadreetat(self.cadreetataction)
        self.lbselectecible=Label(self.cadreetatmsg,text="Choisir cible",bg="darkgrey")
        self.lbselectecible.pack(fill="x")

        self.nomcadreplanete = Label(self.cadreetatactionplanete, text = "--RESSOURCES PLANÈTE--")
        self.nomcadreplanete.pack(fill="x")
        
        self.nom2cadreplanete = Label(self.cadreetatactionplanete, text = "--EXPLOITABLE | UTILISABLE--")
        self.nom2cadreplanete.pack(fill="x")
        #self.canevas.create_image(x,y,image=self.images["scout"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
        #self.canevas.create_image(0,0, image=self.parent.images["wood"])
        #Tkinter.Label(root, image=tkimage, text="Update User", compound=Tkinter.CENTER).pack() # Put it in the display window

        self.woodplanete=Label(self.cadreetatactionplanete,image=self.parent.images["wood"], text="WOOD : ", compound=TOP, bg="yellow")#, image=goldimage)
        self.woodplanete.pack(fill="x")

        self.metalplanete=Label(self.cadreetatactionplanete,image=self.parent.images["metal"], text="METAL : ", compound=TOP,bg="purple", fg="white")
        self.metalplanete.pack(fill="x")

        self.foodplanete=Label(self.cadreetatactionplanete,image=self.parent.images["food"], text="FOOD : ", compound=TOP,bg="pink")
        self.foodplanete.pack(fill="x")

        self.populationplanete=Label(self.cadreetatactionplanete,image=self.parent.images["population"], text="POPULATION :",compound=TOP, bg="white")
        self.populationplanete.pack(fill="x")

        self.btnvuesysteme=Button(self.cadreetataction,text="Voir System",command=self.voirsysteme)
        self.btnvuesysteme.pack(side=BOTTOM)

        #CADRE ACTIONBATIMENT
        self.nomcadrebatiment = Label(self.cadreetatactionbatiment, text = "Option batiment", bg= "Blue")
        self.nomcadrebatiment.pack(fill="x")
       #-------------Math-------------
        #self.nomcadrebatiment = Label(self.cadreetatactibatiment, text = "Option Marcher", bg="Green")
       # self.nomcadrebatiment.pack(fill="x")
       
        #self.btnCreerUnite
        self.btncreerunite=Button(self.cadreetatactionbatiment,image = self.parent.images["btnuniteT"],text="Creer Unité", compound=TOP,command=self.creerunite)
        self.btncreerunite.pack(fill="x")
        
         #-----MathLau------------------
        #self.nomcadrebatiment = Label(self.cadreetatactionbatiment, text ="Option marcher" , bg="Green")
        #self.btnEchange = Button(self.cadreetatactionbatiment,image = self.parent.images["btnEchange"],text="Echange Ressources", compound=TOP,command=self.creerMarcher)
        #self.btnEchange.pack(fill="x")

    def creermine(self):
        """if len(self.selected) >= 2:
            x = self.canevas.getx(self.selected[0])
            y = self.canevas.gety(self.selected[0])
            x1 = self.canevas.getx(self.selected[1])
            y1 = self.canevas.gety(self.selected[1])"""
        self.parent.contructObjet="mine"

    def creerbaraque(self):
        self.parent.contructObjet="baraque"
        
    def creerferme(self):
        self.parent.contructObjet="ferme"

    def creerunite(self):
        self.parent.contructObjet="unite"

    def creermanufacture(self):
        pass
    
    def creerlabo(self):
        self.parent.contructObjet="labo"
        
    def creerwood(self):
        self.parent.contructObjet="wood"

    def creerchurch(self):
        self.parent.contructObjet="church"
    
#-----------mathLau--------------
    def creerMarcher(self):
        self.parent.contructObjet="marcher"
    #-----------mathLau--------------
    def voirsysteme(self):
        for i in self.modele.joueurs[self.parent.nom].systemesvisites:
            if i==self.systeme:
                self.parent.voirsysteme(i)
                
    """def afficherdecor(self,s,p):
        self.creerimagefond()
        self.affichermodelestatique(s,p)

    def creerimagefond(self): #NOTE - au lieu de la creer a chaque fois on aurait pu utiliser une meme image de fond cree avec PIL
        imgfondpil = Image.open("images/landscape.jpg")
        imgfondpil = imgfondpil.resize((self.hauteur, self.largeur), Image.ANTIALIAS)
        self.images["fond"] = ImageTk.PhotoImage(imgfondpil)
        self.canevas.create_image(self.largeur/2,self.hauteur/2,image=self.images["fond"])"""

    def initplanete(self,sys,plane):
        s=None
        p=None

        for visitedSystem in self.modele.joueurs[self.parent.nom].systemesvisites:
            if visitedSystem == sys:
                s = visitedSystem
                for planettes in self.modele.joueurs[self.parent.nom].systemesvisites[visitedSystem].planetes:
                    if planettes == plane:
                        p = planettes
                        break

        self.systemeid = sys
        self.planeteid = plane
        print("s = ", s, "p = ", p)
        print("sys = ", sys, "plane = ", plane)
        self.affichermodelestatique(sys,plane)

    def affichermodelestatique(self,s,p):
        self.chargeimages()
        xl=self.largeur/2
        yl=self.hauteur/2
        mini=2
        UAmini=4
        #for i in p.infrastructures:
            #pass
        print("posX :"+str(self.modele.systemes[s].planetes[p].posXatterrissage))
        print("posY :"+str(self.modele.systemes[s].planetes[p].posYatterrissage))
        self.canevas.create_image(self.modele.systemes[s].planetes[p].posXatterrissage,self.modele.systemes[s].planetes[p].posYatterrissage,image=self.images["ville"])
        
        
        for i in range(0,10):  
            x = self.canevas.canvasx(self.modele.systemes[s].planetes[p].posXatterrissage + (i*30))
            y = self.canevas.canvasy(self.modele.systemes[s].planetes[p].posYatterrissage+50)
            unite=Unite(self,self.parent.parent.monnom,s,p,x,y)
            self.modele.joueurs[unite.joueur].uniteContruite[unite.id] = unite
            ##elf.modele.systemes[s].planetes[p].infrastructures.append(unite) 
            self.canevas.create_image(x,y,image=self.images["uniteT"], tags = ("unite", self.systeme, self.planete, unite.id, unite.joueur))
            
        canl=int(self.modele.systemes[s].planetes[p].posXatterrissage-100)/self.largeur
        canh=int(self.modele.systemes[s].planetes[p].posYatterrissage-100)/self.hauteur
        self.canevas.xview(MOVETO,canl)
        self.canevas.yview(MOVETO, canh)

    def chargeimages(self):
        im = Image.open("./images/ville_100.png")
        self.images["ville"] = ImageTk.PhotoImage(im)
        
        im = Image.open("./images/batiments/golem.png")
        im = im.resize((100,100))
        self.images["mine"] = ImageTk.PhotoImage(im)
                # Image Caserne
        
        im = Image.open("./images/batiments/BarackObama.png")
        im = im.resize((100,100))
        self.images["baraque"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/Unite/uniteTerrestre.png")
        im = im.resize((25,25))
        self.images["uniteT"] = ImageTk.PhotoImage(im)
        
        #####----------------------------------MATLAU
        
        im = Image.open("./images/batiments/genome.png")
        im = im.resize((200,200))
        self.images["labo"] = ImageTk.PhotoImage(im)
        
        im = Image.open("./images/batiments/ham.png")
        im = im.resize((100,100))
        self.images["ferme"] = ImageTk.PhotoImage(im)
        
        im = Image.open("./images/batiments/wood.png")
        im = im.resize((100,100))
        self.images["wood"] = ImageTk.PhotoImage(im)
        
        im = Image.open("./images/batiments/bouddha.png")
        im = im.resize((100,100))
        self.images["church"] = ImageTk.PhotoImage(im)
        
     #----------------mathLau-----------------   
        #im = Image.open("./images/Timer/timer.png")
       # im = im.resize((25,25))
        #self.images["timer"] = ImageTk.PhotoImage(im)
        
        
        im = Image.open("./images/batiments/marcher.png")
        im = im.resize((100,100))
        self.images["marcher"] = ImageTk.PhotoImage(im)
    #-----------------mathLau---------------------------
    
    def afficherdecor(self):
        pass

    def creervaisseau(self):
        pass

    def creerstation(self):
        print("Creer station EN CONSTRUCTION")

    def afficherpartie(self,mod):
        if self.maselection:
            if self.maselection[0] == "infrastructure":
                self.montrebatimentselection(self.cadreetatactionbatiment)
        elif self.maselection == None:
                self.montrebatimentselection(None)

        self.montreplaneteselection()
        self.woodplanete.config(text = "WOOD : " + str(self.modele.systemes[self.systeme].planetes[self.planete].wood) + ' | ' + str(self.modele.systemes[self.systeme].planetes[self.planete].woodExp))
        self.metalplanete.config(text = "METAL : " + str(self.modele.systemes[self.systeme].planetes[self.planete].ressourcemine ) + ' | ' + str(self.modele.systemes[self.systeme].planetes[self.planete].mineExp))
        self.foodplanete.config(text = "FOOD : " + str(self.modele.systemes[self.systeme].planetes[self.planete].food) + ' | ' + str(self.modele.systemes[self.systeme].planetes[self.planete].foodExp))
        self.populationplanete.config(text = "POPULATION : " + str(self.modele.systemes[self.systeme].planetes[self.planete].population))

    def montreplaneteselection(self):
        self.changecadreetatplanete(self.cadreetatactionplanete)

    def montrebatimentselection(self,cadre):
        self.changecadreetatbatiment(cadre)

    def changerproprietaire(self,prop,couleur,systeme):
        pass

    def afficherselection(self):
        pass
    
            ##-----------------------------matlau
    def montresystemeselection(self):
        self.changecadreetat(self.cadreetataction)

    def montrevaisseauxselection(self):
        self.changecadreetat(self.cadreetatmsg)

    def afficherartefacts(self,joueurs):
        pass #print("ARTEFACTS de ",self.nom)

    def cliquerminimap(self,evt):
        x=evt.x
        y=evt.y
        xn=self.largeur/int(self.minimap.winfo_width())
        yn=self.hauteur/int(self.minimap.winfo_height())

        ee=self.canevas.winfo_width()
        ii=self.canevas.winfo_height()
        eex=int(ee)/self.largeur/2
        eey=int(ii)/self.hauteur/2

        self.canevas.xview(MOVETO, (x*xn/self.largeur)-eex)
        self.canevas.yview(MOVETO, (y*yn/self.hauteur)-eey)
