# -*- encoding: utf-8 -*-

from Perspective import Perspective
from tkinter import *
from PIL import Image,ImageDraw, ImageTk
import random
from subprocess import Popen
from helper import Helper as hlp
import math

class VueGalaxie(Perspective):
    def __init__(self,parent):
        Perspective.__init__(self,parent)
        self.modele=self.parent.modele
        self.AL2pixel=100

        self.largeur=self.modele.diametre*self.AL2pixel
        self.hauteur=self.largeur

        self.canevas.config(scrollregion=(0,0,self.largeur,self.hauteur))

        self.labid.bind("<Button>",self.identifierplanetemere)

        #Bouton pour creer un vaisseau de Jean-Marc
        #self.btncreervaisseau=Button(self.cadreetataction,text="Creer Vaisseau",command=self.creervaisseau)
        #self.btncreervaisseau.pack()

        #Kassem and Humza
        ####################################################################################################

        #--VAISSEAU SCOUT
        self.btncreervaisseau=Button(self.cadreetataction,text="Vaisseau Scout",command=self.creervaisseauscout)
        self.btncreervaisseau.pack(fill="x")

        #--VAISSEAU ATTAQUE
        self.btncreervaisseau=Button(self.cadreetataction,text="Vaisseau Attaque",command=self.creervaisseauattaque)
        self.btncreervaisseau.pack(fill="x")

        #--VAISSEAU BOMBARDIER
        self.btncreervaisseau=Button(self.cadreetataction,text="Vaisseau Bombardier",command=self.creervaisseaubombardier)
        self.btncreervaisseau.pack(fill="x")

        #VAISSEAU CARGO
        self.btncreervaisseau=Button(self.cadreetataction,text="Vaisseau Cargo",command=self.creervaisseaucargo)
        self.btncreervaisseau.pack(fill="x")

        #VAISSEAU TRANSPORTEUR
        self.btncreervaisseau=Button(self.cadreetataction,text="Vaisseau Transporteur",command=self.creervaisseautransporteur)
        self.btncreervaisseau.pack(fill="x")

        #CREATION STATION
        self.btncreerstation=Button(self.cadreetataction,text="Station",command=self.creerstation)
        self.btncreerstation.pack(fill="x")
        
        self.btnvuesysteme=Button(self.cadreetataction,text="Voir Systeme",command=self.voirsysteme)
        self.btnvuesysteme.pack(fill="x",side=BOTTOM)

        self.lbselectecible=Label(self.cadreetatmsg,text="Choisir cible",bg="darkgrey")
        self.lbselectecible.pack()

                
    def voirsysteme(self,systeme=None):
        if systeme:
            # AVEC SYSTEM
            sid=systeme.id
            for i in self.modele.joueurs[self.parent.nom].systemesvisites:
                if i==sid:
                    s=i
                    break
            # NOTE passer par le serveur est-il requis ????????????
            self.parent.parent.visitersysteme(sid)
            self.parent.voirsysteme(s) #normalement devrait pas planter
        else:
            # SANS SYSTEM
            selectedTag = self.canevas.gettags(self.selected)
            if "systeme" in selectedTag:
                idSystem = selectedTag[1]
                self.parent.parent.visitersysteme(idSystem)
                self.parent.voirsysteme(idSystem)

    def voirsysteme1(self,systeme=None):
        if systeme==None:
            # SANS SYSTEME
            if self.parent.nom in self.maselection and "systeme" in self.maselection :
                sid=self.maselection[2]
                for i in self.modele.joueurs[self.parent.nom].systemesvisites:
                    if i==sid:
                        s=i
                        break
                self.parent.parent.visitersysteme(sid)
                self.parent.voirsysteme(s) #normalement devrait pas planter
        else:
            # AVEC SYSTEME
            sid=systeme.id
            for i in self.modele.joueurs[self.parent.nom].systemesvisites:
                if i==sid:
                    s=i
                    break
            # NOTE passer par le serveur est-il requis ????????????
            self.parent.parent.visitersysteme(sid)
            self.parent.voirsysteme(s) #normalement devrait pas planter

        """
        def chargeimages(self):
        #code à JMD
        im = Image.open("./images/chasseur.png")
        self.images["chasseur"] = ImageTk.PhotoImage(im)

        #IMAGE RESSOURCES
        im = Image.open("./images/ressources/food.png")
        im = im.resize((50,50))
        self.images["food"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/ressources/metal.png")
        im = im.resize((50,50))
        self.images["metal"] = ImageTk.PhotoImage(im)

        im = Image.open("./images/ressources/wood.png")
        im = im.resize((50,50))
        self.images["wood"] = ImageTk.PhotoImage(im)


        #Ajout de Humza et Kassem
        angle = 0
        # angle 0 = angle 90 en mathematique
        while angle < 360 :
        #Scout
            im = Image.open("./images/vaisseau_scout1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["scout"+str(angle)] = ImageTk.PhotoImage(im)
        #Attaque
            im = Image.open("./images/vaisseau_attaque1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["attaque"+str(angle)] = ImageTk.PhotoImage(im)
        #Bombardier
            im = Image.open("./images/vaisseau_bombardier1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["bombardier"+str(angle)] = ImageTk.PhotoImage(im)
        #Cargo
            im = Image.open("./images/vaisseau_cargo1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["cargo"+str(angle)] = ImageTk.PhotoImage(im)
        #Transporteur (meme image que le cargo)
            im = Image.open("./images/vaisseau_cargo1/"+str(angle)+".png")
            im = im.resize((50,50))
            self.images["transporteur"+str(angle)] = ImageTk.PhotoImage(im)
            angle = angle + 45
            angle = angle + 45
    """

    def afficherdecor(self):
        self.creerimagefond()
        self.affichermodelestatique()

    def creerimagefond(self): #NOTE - au lieu de la creer a chaque fois on aurait pu utiliser une meme image de fond cree avec PIL
        imgfondpil = Image.open("images/nebula1.png")
        imgfondpil = imgfondpil.resize((self.hauteur, self.largeur), Image.ANTIALIAS)
        """draw = ImageDraw.Draw(imgfondpil)
        for i in range(self.largeur*2):
            x=random.randrange(self.largeur)
            y=random.randrange(self.hauteur)
            #draw.ellipse((x,y,x+1,y+1), fill="white")
            draw.ellipse((x,y,x+0.1,y+0.11), fill="white")"""
        self.images["fond"] = ImageTk.PhotoImage(imgfondpil)
        self.canevas.create_image(self.largeur/2,self.hauteur/2,image=self.images["fond"])

    def affichermodelestatique(self):
        mini=self.largeur/200
        e=self.AL2pixel
        me=200/self.modele.diametre
        cles=list(self.modele.systemes)
        cles.sort()
        for i in cles:
            i=self.modele.systemes[i]
            t=i.etoile.taille*3
            if t<10:
                t=10
            self.canevas.create_oval((i.x*e)-t,(i.y*e)-t,(i.x*e)+t,(i.y*e)+t,fill="grey80",
                                     tags=("systeme",i.id,"inconnu"))
            # NOTE pour voir les id des objets systeme, decommentez la ligne suivantes
            self.canevas.create_text((i.x*e)-t,(i.y*e)-(t*2),text=str(i.id),fill="white")
            
            for i in self.modele.joueurscles:
                couleur=self.modele.joueurs[i].couleur
                m=10
                for j in self.modele.joueurs[i].systemesvisites:
                    s=self.canevas.find_withtag(j)
                    self.canevas.addtag_withtag(i, s)
                    self.canevas.itemconfig(s,fill=couleur)
    
                    jj = self.modele.joueurs[i].systemesvisites[j] #self.modele.systemes[j]
                    self.minimap.create_oval((jj.x*me)-m,(jj.y*me)-m,(jj.x*me)+m,(jj.y*me)+m,fill=couleur)

    def affichermodelestatique1(self):
        mini=self.largeur/200
        e=self.AL2pixel
        me=200/self.modele.diametre
        cles=list(self.modele.systemes)
        cles.sort()
        for i in cles:
            i=self.modele.systemes[i]
            t=i.etoile.taille*3
            if t<10:
                t=10
            self.canevas.create_oval((i.x*e)-t,(i.y*e)-t,(i.x*e)+t,(i.y*e)+t,fill="grey80",
                                     tags=("systeme",i.id,str(i.x),str(i.y)))
            # NOTE pour voir les id des objets systeme, decommentez la ligne suivantes
            #self.canevas.create_text((i.x*e)-t,(i.y*e)-(t*2),text=str(i.id),fill="white")

        for i in self.modele.joueurscles:
            couleur=self.modele.joueurs[i].couleur
            m=10
            for j in self.modele.joueurs[i].systemesvisites:
                s=self.canevas.find_withtag(j)
                self.canevas.addtag_withtag(i, s)
                self.canevas.itemconfig(s,fill=couleur)

                jj = self.modele.joueurs[i].systemesvisites[j] #self.modele.systemes[j]
                self.minimap.create_oval((jj.x*me)-m,(jj.y*me)-m,(jj.x*me)+m,(jj.y*me)+m,fill=couleur)
            ##-------matlau

    # ************************ FIN DE LA SECTION D'AMORCE DE LA PARTIE

    def identifierplanetemere(self,evt):
        j=self.modele.joueurs[self.parent.nom]
        couleur=j.couleur
        x=j.systemeorigine.x*self.AL2pixel
        y=j.systemeorigine.y*self.AL2pixel
        id=j.systemeorigine.id
        # Taille des planetes
        t=10
        self.canevas.create_oval(x-t,y-t,x+t,y+t,dash=(3,3),width=2,outline=couleur,
                                 tags=(self.parent.nom,"selecteur",id,""))
        xx=x/self.largeur
        yy=y/self.hauteur
        ee=self.canevas.winfo_width()
        ii=self.canevas.winfo_height()
        eex=int(ee)/self.largeur/2
        self.canevas.xview(MOVETO, xx-eex)
        eey=int(ii)/self.hauteur/2
        self.canevas.yview(MOVETO, yy-eey)

    def creervaisseauscout(self):
        if self.maselection:
            self.parent.parent.creervaisseauscout(self.maselection[1])
            self.maselection=None
            self.canevas.delete("selecteur")

    def creervaisseauattaque(self):
        if self.maselection:
            self.parent.parent.creervaisseauattaque(self.maselection[1])
            self.maselection=None
            self.canevas.delete("selecteur")

    def creervaisseaubombardier(self):
        if self.maselection:
            self.parent.parent.creervaisseaubombardier(self.maselection[1])
            self.maselection=None
            self.canevas.delete("selecteur")

    def creervaisseaucargo(self):
        if self.maselection:
            self.parent.parent.creervaisseaucargo(self.maselection[1])
            self.maselection=None
            self.canevas.delete("selecteur")

    def creervaisseautransporteur(self):
        if self.maselection:
            self.parent.parent.creervaisseautransporteur(self.maselection[1])
            self.maselection=None
            self.canevas.delete("selecteur")


    def creerstation(self):
        if self.maselection:
            self.parent.parent.creerstation(self.maselection[1])
            self.maselection=None
            self.canevas.delete("selecteur")

    def afficherpartie(self,mod):
        self.canevas.delete("artefact")
        self.canevas.delete("pulsar")
        #self.afficherselection()

        e=self.AL2pixel
        for i in mod.joueurscles:
            i=mod.joueurs[i]
            jcle=list(i.vaisseauxinterstellaires.keys())
            jv=i.vaisseauxinterstellaires
            for k in jcle:
                j=jv[k]
                jx=j.x*e
                jy=j.y*e
                x2,y2=hlp.getAngledPoint(j.angletrajet,8,jx,jy)
                x1,y1=hlp.getAngledPoint(j.angletrajet,4,jx,jy)
                x0,y0=hlp.getAngledPoint(j.angleinverse,4,jx,jy)
                x,y=hlp.getAngledPoint(j.angleinverse,7,jx,jy)
                self.canevas.create_line(x,y,x0,y0,fill="yellow",width=3,
                                         tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                self.canevas.create_line(x0,y0,x1,y1,fill=i.couleur,width=4,
                                         tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                self.canevas.create_line(x1,y1,x2,y2,fill="red",width=2,
                                         tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))

                 #Humza Kassem dessine vaisseau
                #Conversion de rad a deg (Fuck les radians lol)
                if j.angletrajet >= 0 :
                    anglerotation = j.angletrajet*180/3.1416
                else :
                    anglerotation = -1*(j.angletrajet)*180/3.1416

                if anglerotation > 337.5 or anglerotation <= 22.5 :
                    anglerotation = 0
                elif anglerotation <= 67.5:
                    anglerotation = 45
                elif anglerotation <= 112.5 :
                    anglerotation = 90
                elif anglerotation <= 157.5 :
                    anglerotation = 135
                elif anglerotation <= 202.5 :
                    anglerotation = 180
                elif anglerotation <= 247.5 :
                    anglerotation = 225
                elif anglerotation <= 292.5 :
                    anglerotation = 270
                elif anglerotation <= 337.5 :
                    anglerotation = 315

                #"vaisseauinterstellaire"
                if j.type == "scout":
                    self.canevas.create_image(x,y,image=self.parent.images["scout"+str(anglerotation)],tags=("vaisseauinterstellaire",j.id,j.proprietaire))
                if j.type == "attaque":
                    self.canevas.create_image(x,y,image=self.parent.images["attaque"+str(anglerotation)],tags=("vaisseauinterstellaire",j.id,j.proprietaire))
                if j.type == "bombardier":
                    self.canevas.create_image(x,y,image=self.parent.images["bombardier"+str(anglerotation)],tags=("vaisseauinterstellaire",j.id,j.proprietaire))
                if j.type == "cargo":
                    self.canevas.create_image(x,y,image=self.parent.images["cargo"+str(anglerotation)],tags=("vaisseauinterstellaire",j.id,j.proprietaire))
                if j.type == "transporteur":
                    self.canevas.create_image(x,y,image=self.parent.images["transporteur"+str(anglerotation)],tags=("vaisseauinterstellaire",j.id,j.proprietaire))


        for i in mod.pulsars:
            t=i.taille
            self.canevas.create_oval((i.x*e)-t,(i.y*e)-t,(i.x*e)+t,(i.y*e)+t,fill="orchid3",dash=(1,1),
                                                 outline="maroon1",width=2,
                                     tags=("pulsar",i.id))


    def afficherpartie1(self,mod):
        self.canevas.delete("artefact")
        self.canevas.delete("pulsar")
        self.afficherselection()

        e=self.AL2pixel
        for i in mod.joueurscles:
            i=mod.joueurs[i]
            for j in i.vaisseauxinterstellaires:

                #anglerotation
                anglerotation =  int (math.degrees(j.angletrajet))
                if j.angletrajet < 0 :
                   anglerotation = int (math.degrees(2*math.pi + j.angletrajet))

                """
                anglerotation = int((anglerotation)/45)*45
                anglerotation = anglerotation

                if anglerotation >= 360 :
                    anglerotation = anglerotation - 360
                """
                #print("ANGLE" , j.angletrajet, math.degrees(j.angletrajet),anglerotation)
                jx=j.x*e
                jy=j.y*e
                x2,y2=hlp.getAngledPoint(j.angletrajet,8,jx,jy)
                x1,y1=hlp.getAngledPoint(j.angletrajet,4,jx,jy)
                x0,y0=hlp.getAngledPoint(j.angleinverse,4,jx,jy)
                x,y=hlp.getAngledPoint(j.angleinverse,7,jx,jy)

                #JMD dessine le vaisseau ici
                """
                self.canevas.create_line(x,y,x0,y0,fill="yellow",width=3,
                                         tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                self.canevas.create_line(x0,y0,x1,y1,fill=i.couleur,width=4,
                                         tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                self.canevas.create_line(x1,y1,x2,y2,fill="red",width=2,
                                         tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                """

                #Humza Kassem dessine vaisseau
                #Conversion de rad a deg (Fuck les radians lol)
                """
                if j.angletrajet >= 0 :
                    anglerotation = j.angletrajet*180/3.1416
                else :
                    anglerotation = -1*(j.angletrajet)*180/3.1416
                    print(anglerotation)
               """
                #print (j.angletrajet, anglerotation)


                if anglerotation > 337.5 or anglerotation <= 22.5 :
                    anglerotation = 0
                elif anglerotation <= 67.5:
                    anglerotation = 315
                elif anglerotation <= 112.5 :
                    anglerotation = 270
                elif anglerotation <= 157.5 :
                    anglerotation = 225
                elif anglerotation <= 202.5 :
                    anglerotation = 180
                elif anglerotation <= 247.5 :
                    anglerotation = 135
                elif anglerotation <= 292.5 :
                    anglerotation = 90
                elif anglerotation <= 337.5 :
                    # Branche mig
                    #anglerotation = 315
                    # Branche kassem
                    anglerotation = 45
                #print ("ANGLE DE ROTATION" , anglerotation)
                if j.type == "scout":
                    self.canevas.create_image(x,y,image=self.parent.images["scout"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "attaque":
                    self.canevas.create_image(x,y,image=self.parent.images["attaque"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "bombardier":
                    self.canevas.create_image(x,y,image=self.parent.images["bombardier"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "cargo":
                    self.canevas.create_image(x,y,image=self.parent.images["cargo"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))
                if j.type == "transporteur":
                    self.canevas.create_image(x,y,image=self.parent.images["transporteur"+str(anglerotation)],tags=(j.proprietaire,"vaisseauinterstellaire",j.id,"artefact"))


        for i in mod.pulsars:
            t=i.taille
            self.canevas.create_oval((i.x*e)-t,(i.y*e)-t,(i.x*e)+t,(i.y*e)+t,fill="orchid3",dash=(1,1),
                                                 outline="maroon1",width=2,
                                     tags=("inconnu","pulsar",i.id))

    def changerproprietaire(self,prop,couleur,systeme):
        #lp=self.canevas.find_withtag(systeme.id)
        self.canevas.addtag_withtag(prop,systeme.id)

    def changerproprietaire1(self,prop,couleur,systeme):
        id=str(systeme.id)
        lp=self.canevas.find_withtag(id)
        self.canevas.itemconfig(lp[0],fill=couleur)
        t=(prop,"systeme",id,"systemevisite",str(len(systeme.planetes)),systeme.etoile.type)
        self.canevas.itemconfig(lp[0],tags=t)

    def afficherselection(self):

        self.canevas.delete("selecteur")
        if self.maselection!=None:
            joueur=self.modele.joueurs[self.parent.nom]

            e=self.AL2pixel
            if self.maselection[1]=="systeme":
                for i in joueur.systemesvisites:
                    if i == self.maselection[2]:
                        x=joueur.systemesvisites[i].x
                        y=joueur.systemesvisites[i].y
                        t=10
                        self.canevas.create_oval((x*e)-t,(y*e)-t,(x*e)+t,(y*e)+t,dash=(2,2),
                                                 outline=joueur.couleur,
                                                 tags=("select","selecteur"))
            elif self.maselection[1]=="vaisseauinterstellaire":
                for i in joueur.vaisseauxinterstellaires:
                    if i == self.maselection[2]:
                        x=joueur.vaisseauxinterstellaires[i].x
                        y=joueur.vaisseauxinterstellaires[i].y
                        t=10
                        self.canevas.create_rectangle((x*e)-t,(y*e)-t,(x*e)+t,(y*e)+t,dash=(2,2),
                                                      outline=joueur.couleur,
                                                      tags=("select","selecteur"))

    def afficherselection1(self):
        self.canevas.delete("selecteur")
        if len(self.maselection) > 0:
            joueur=self.modele.joueurs[self.parent.nom]

            e=self.AL2pixel
            if "systeme" in self.maselection:
                for i in joueur.systemesvisites:
                    if i.id in self.maselection:
                        x=i.x
                        y=i.y
                        t=10
                        self.canevas.create_oval((x*e)-t,(y*e)-t,(x*e)+t,(y*e)+t,dash=(2,2),
                                                 outline=joueur.couleur,
                                                 tags=("select","selecteur"))
            elif "vaisseauinterstellaire" in self.maselection:
                for i in joueur.vaisseauxinterstellaires:
                    if i.id == self.maselection[2]:
                        x=i.x
                        y=i.y
                        t=10
                        self.canevas.create_rectangle((x*e)-t,(y*e)-t,(x*e)+t,(y*e)+t,dash=(2,2),
                                                      outline=joueur.couleur,
                                                      tags=("select","selecteur"))

    #def cliquervue(self,evt):
    #    self.changecadreetat(None)
    #    t=self.canevas.gettags("current")
    #    if t and t[0]!="current":
#
#            if t[1]=="vaisseauinterstellaire":
#                print("IN VAISSEAUINTERSTELLAIRE",t)
#                self.maselection=[self.parent.nom,t[1],t[2]]
#                self.montrevaisseauxselection()
#
#            elif t[1]=="systeme":
#                print("IN SYSTEME",t)
#                if self.maselection and self.maselection[1]=="vaisseauinterstellaire":
#                    print("IN systeme + select VAISSEAUINTERSTELLAIRE")
#                    self.parent.parent.ciblerdestination(self.maselection[2],t[2])
#                elif self.parent.nom in t:
#                    print("IN systeme  PAS SELECTION")
#                    self.maselection=[self.parent.nom,t[1],t[2]]
#                    self.montresystemeselection()
#                else:
#                    print("IN systeme + RIEN")
#                    self.maselection=None
#                    self.lbselectecible.pack_forget()
#                    self.canevas.delete("selecteur")
#            else:
#                print("Objet inconnu")
#        else:
#            print("Region inconnue")
#            self.maselection=None
#            self.lbselectecible.pack_forget()
#            self.canevas.delete("selecteur")


    def montresystemeselection(self):
        self.changecadreetat(self.cadreetataction)

    def montrevaisseauxselection(self):
        self.changecadreetat(self.cadreetatmsg)

    def afficherartefacts(self,joueurs):
        pass #print("ARTEFACTS de ",self.nom)

    def cliquerminimap(self,evt):
        x=evt.x
        y=evt.y
        xn=self.largeur/int(self.minimap.winfo_width())
        yn=self.hauteur/int(self.minimap.winfo_height())

        ee=self.canevas.winfo_width()
        ii=self.canevas.winfo_height()
        eex=int(ee)/self.largeur/2
        eey=int(ii)/self.hauteur/2

        self.canevas.xview(MOVETO, (x*xn/self.largeur)-eex)
        self.canevas.yview(MOVETO, (y*yn/self.hauteur)-eey)
