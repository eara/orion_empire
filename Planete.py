# -*- encoding: utf-8 -*-

from Id import Id
import random
from Ville import Ville

class Planete():
    def __init__(self,parent,type,dist,taille,angle):
        self.id=Id.prochainid()
        self.parent=parent
        self.posXatterrissage=random.randrange(100,900)
        self.posYatterrissage=random.randrange(100,1100)
        self.infrastructures=[Ville(self)]
        self.proprietaire="inconnu"
        self.visiteurs={}
        self.distance=dist
        self.type=type
        self.taille=taille
        self.angle=angle

        self.station = None
        self.stationexiste = False
        self.coutstation = 500

        #Variable temporaire en attendant le code officiel
        #self.ressourcemine=1000000000
        ##----------------------------matlau
        self.wood = random.randrange(1000,9999)
        self.food = random.randrange(1000,9999)
        self.ressourcemine = random.randrange(1000,9999)
        self.population = 10
        self.populationMax = 50
        self.woodExp = 1000
        self.foodExp = 0
        self.mineExp = 1000
        ##----------------------------matlau

    def creerstation(self):
        if not self.stationexiste:
            if self.ressourcemine > self.coutstation :
                self.stationexiste = True
                self.ressourcemine = self.ressourcemine - self.coutstation
                self.station = Station(proprietaire,self)
