# -*- encoding: utf-8 -*-

from Id import Id
import random
from helper import Helper as hlp
import math

class Vaisseau():
    def __init__(self,nom,systeme,vitesse, taille,type):
        self.id=Id.prochainid()
        self.proprietaire=nom
        self.taille=taille
        self.base=systeme
        self.angletrajet=0
        self.angleinverse=0

        self.x=self.base.x
        self.y=self.base.y
        self.energie=100
        self.cible=None
        self.newX = self.x
        self.newY = self.y
        #Modifier la vitesse qui etait generer de facon random avant
        self.vitesse=vitesse
         #Modification de la taille pourqu'elle soit relative au type de vaisseau
        #self.taille=16
        self.taille=taille
        self.type = type
        #Variable pour la position systeme
        self.ciblesysteme=None
        self.angletrajetsysteme=0
        self.angleinversesysteme=0
        self.systemex = 20
        self.systemey = 20

    def avancer(self):
        rep=None
        if self.cible:
            x=self.cible.x
            y=self.cible.y
            self.x,self.y=hlp.getAngledPoint(self.angletrajet,self.vitesse,self.x,self.y)
            if hlp.calcDistance(self.x,self.y,x,y) <=self.vitesse:
                rep=self.cible
                self.base=self.cible
                self.cible=None
            return rep

    def avancersysteme(self):
        rep=None
        if self.ciblesysteme:
            x=self.ciblesysteme.x
            y=self.ciblesysteme.y
            self.systemex,self.systemey=hlp.getAngledPoint(self.angletrajetsysteme,self.vitesse,self.systemex,self.systemey)
            if hlp.calcDistance(self.systemex,self.systemeyy,x,y) <=self.vitesse:
                rep=self.ciblesysteme
                self.base=self.ciblesysteme
                self.ciblesysteme=None
            return rep

    def ciblerdestination(self,p):
        self.cible=p
        self.angletrajet=hlp.calcAngle(self.x,self.y,p.x,p.y)
        self.angleinverse=math.radians(math.degrees(self.angletrajet)+180)
        dist=hlp.calcDistance(self.x,self.y,p.x,p.y)
        #print("Distance",dist," en ", int(dist/self.vitesse))

    def ciblerdestination1(self,p):
        self.cible=p
        self.angletrajet=hlp.calcAngle(self.x,self.y,p.x,p.y)
        self.angleinverse=math.radians(math.degrees(self.angletrajet)+180)
        dist=hlp.calcDistance(self.x,self.y,p.x,p.y)
        #print("Distance",dist," en ", int(dist/self.vitesse))


    def ciblerdestinationsysteme(self,p):
        print(p)
        self.ciblesysteme=p
        self.angletrajetsysteme=hlp.calcAngle(self.systemex,self.systemey,p.x,p.y)
        self.angleinversesysteme=math.radians(math.degrees(self.angletrajetsysteme)+180)
        dist=hlp.calcDistance(self.systemex,self.systemey,p.x,p.y)
        #print("Distance",dist," en ", int(dist/self.vitesse))

class VaisseauScout(Vaisseau):
    def __init__(self,nom,systeme):
        Vaisseau.__init__(self, nom, systeme, 0.01 ,10,"scout")
        self.forceattaque = 5

    def invisibilite(self):
        pass

class VaisseauBombardier(Vaisseau):
    def __init__(self,nom,systeme):
        Vaisseau.__init__(self, nom, systeme, 0.003, 20,"bombardier")
        self.forceattaque = 10
        self.forcebombarde = 50
        self.diametrebombarde = 5

    def bombarde(self):
        pass

class VaisseauAttaque(Vaisseau):
    def __init__(self,nom,systeme):
        Vaisseau.__init__(self, nom, systeme, 0.007, 15,"attaque")
        self.forceattaque = 20

class VaisseauCargo(Vaisseau):
    def __init__(self,nom,systeme):
        Vaisseau.__init__(self, nom, systeme, 0.005, 50,"cargo")
        self.capacitemine = 500
        self.capacitebois = 500
        self.capacitenourriture = 1000
        self.stockmine = 0
        self.stockbois = 0
        self.stocknourriture = 0

    def charger_ressource(self):
        pass

    def decharger_ressource(self):
        pass

class VaisseauTransporteur(Vaisseau):
    def __init__(self,nom,systeme):
        Vaisseau.__init__(self, nom, systeme, 0.005, 30,"transporteur")
        self.capaciteunite = 50
        self.unite = []

    def charger_unite(self):
        pass

    def decharger_unite(self):
        pass
