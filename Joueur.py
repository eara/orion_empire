# -*- encoding: utf-8 -*-

from Mine import Mine
from Baraque import Baraque
from Unite import Unite
from Labo import Labo
from Ferme import Ferme
from Church import Church
from Wood import Wood
from Marcher import Marcher
from Id import Id
from Vaisseau import *


class Joueur():
    def __init__(self,parent,nom,systemeorigine,couleur):
        self.id=Id.prochainid()
        self.artificiel=0   # IA
        self.parent=parent
        self.nom=nom
        self.systemeorigine=systemeorigine
        self.couleur=couleur
        self.systemesvisites={systemeorigine.id:systemeorigine}
        self.vaisseauxinterstellaires={}
        self.vaisseauxinterplanetaires={}
        self.uniteContruite={}
        self.actions={"creervaisseau":self.creervaisseau,
                      "creervaisseauscout":self.creervaisseauscout,
                      "creervaisseauattaque":self.creervaisseauattaque,
                      "creervaisseaubombardier":self.creervaisseaubombarde,
                      "creervaisseaucargo":self.creervaisseaucargo,
                      "creervaisseautransporteur":self.creervaisseautransporteur,
                      "creerstation":self.creerstation,                 #Ligne rajouter pour creer une station
                      "ciblerdestination":self.ciblerdestination,
                      "ciblerdestinationsysteme":self.ciblerdestinationsysteme,
                      "atterrirplanete":self.atterrirplanete,
                      "visitersysteme":self.visitersysteme,
                      "creermine":self.creermine,
                      "creerbaraque":self.creerbaraque,
                      "creerferme":self.creerferme,
                      "creerunite":self.creerunite,
                      "creerlabo":self.creerlabo,
                      "creerwood":self.creerwood,
                      "creerchurch":self.creerchurch,
                      "creerunite":self.creerunite,
                      "creerMarcher":self.creerMarcher}


    def creermine(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        mine=Mine(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(mine)
        self.parent.parent.affichermine(nom,systemeid,planeteid, mine.id, x, y)

    ##---------------------------------------------------------matlau
    def creerbaraque(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        baraque=Baraque(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(baraque)
        self.parent.parent.afficherbaraque(nom,systemeid,planeteid, baraque.id, x, y)
    
    def creerferme(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        ferme=Ferme(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(ferme)
        self.parent.parent.afficherferme(nom,systemeid,planeteid, ferme.id, x, y)
        

    def creerunite(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        unite=Unite(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(unite)
        self.parent.parent.afficherunite(nom,systemeid,planeteid, unite.id, x, y)
    #----------------------mathLau------------------    
    def creerMarcher(self,listeparams):  
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        marcher=Marcher(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(marcher)
        self.parent.parent.affichermarcher(nom,systemeid,planeteid, marcher.id, x, y)
    
    ##---------------------------------------------------------matlau

    def creerlabo(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        labo=Labo(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(labo)
        self.parent.parent.afficherlabo(nom,systemeid,planeteid, labo.id, x,y)
        
    def creerwood(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        wood=Wood(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(wood)
        self.parent.parent.afficherwood(nom,systemeid,planeteid, wood.id, x,y)
        
    def creerchurch(self,listeparams):
        nom,systemeid,planeteid,x,y=listeparams
        i = self.systemesvisites[systemeid]

        j= i.planetes[planeteid]

        church=Church(self,nom,systemeid,planeteid,x,y)
        j.infrastructures.append(church)
        self.parent.parent.afficherchurch(nom,systemeid,planeteid, church.id, x,y)
        
    def atterrirplanete(self,d):
        nom,systeid,planeid=d
        i = self.systemesvisites[systeid]
        j = i.planetes[planeid]
        i.planetesvisites.append(j)
        if nom==self.parent.parent.monnom:
            self.parent.parent.voirplanete(i.id,j.id)

    def atterrirplanete1(self,d):
        nom,systeid,planeid=d
        for i in self.systemesvisites:
            if i.id==systeid:
                for j in i.planetes:
                    if j.id==planeid:
                        i.planetesvisites.append(j)
                        if nom==self.parent.parent.monnom:
                            self.parent.parent.voirplanete(i.id,j.id)
                        return 1

    def visitersysteme(self,systeme_id):
        idsys = systeme_id[0]
        systeme=self.parent.systemes[idsys]
        self.systemesvisites[idsys] = systeme

    def creervaisseau(self,syst):
        #syst=self.systemesvisites[id]
        i = random.randrange(5)
        
        if i == 1 : 
            v=VaisseauScout(self.nom,syst)
        if i == 2 : 
            v=VaisseauBombardier(self.nom,syst)
        if i == 3 : 
            v=VaisseauAttaque(self.nom,syst)
        if i == 4 : 
            v=VaisseauCargo(self.nom,syst)
        if i == 5 : 
            v=VaisseauTransporteur(self.nom,syst)
            
        v=VaisseauScout(self.nom,syst)
        self.vaisseauxinterstellaires[v.id]=v
        for i in self.parent.systemes:
            if i.id==systeme_id:
                self.systemesvisites.append(i)
                
##-lau---------------------------------------------------------v
    def creerstation(self,id):
        i = self.systemesvisites[id]
        for j in i.planetes:
            if not i.planetes[j].station:
                i.planetes[j].station = True;
                return 1 #pour sortir de la boucle


    def creerstation1(self,id):
        for i in self.systemesvisites:
            if i.id == id :
                for j in i.planetes:
                    if not j.station:
                        j.station = True;
                        print("Création d'une station")
                        return 1 #pour sortir de la boucle

    def creervaisseauscout(self,id):
        i = self.systemesvisites[id]
        #Créer un vaisseau seulement si une station existe
        for j in i.planetes:
            if i.planetes[j].station :
                #v=Vaisseau(self.nom,i,20,20)
                v=VaisseauScout(self.nom,i)
                self.vaisseauxinterstellaires[v.id] = v
                return 1
  ##-lau--------------------------------------------------------^
    def creervaisseauscout1(self,id):
        for i in self.systemesvisites:
            if i.id==id:
                #Créer un vaisseau seulement si une station existe
                for j in i.planetes:
                    if j.station :
                        print("Création d'un vaisseau scout")

                        #v=Vaisseau(self.nom,i,20,20)
                        v=VaisseauScout(self.nom,i)
                        self.vaisseauxinterstellaires.append(v)
                        return 1
##-lau--------------------------------------------------------------------------------------v
    def creervaisseauattaque(self,id):
        i = self.systemesvisites[id]
        #Créer un vaisseau seulement si une station existe
        for j in i.planetes:
            if i.planetes[j].station :
                #v=Vaisseau(self.nom,i,20,20)
                v=VaisseauAttaque(self.nom,i)
                self.vaisseauxinterstellaires[v.id] = v
                return 1

    def creervaisseaubombarde(self,id):
        i = self.systemesvisites[id]
        #Créer un vaisseau seulement si une station existe
        for j in i.planetes:
            if i.planetes[j].station :
                #v=Vaisseau(self.nom,i,20,20)
                v=VaisseauBombardier(self.nom,i)
                self.vaisseauxinterstellaires[v.id] = v
                return 1

    def creervaisseaucargo(self,id):
        i = self.systemesvisites[id]
        #Créer un vaisseau seulement si une station existe
        for j in i.planetes:
            if i.planetes[j].station :
                #v=Vaisseau(self.nom,i,20,20)
                v=VaisseauCargo(self.nom,i)
                self.vaisseauxinterstellaires[v.id] = v
                return 1
        ## commentaire = branche kassem
        #for i in self.systemesvisites: # Remplace le for ci haut non commenté
        #    if i.id==id:
                #Code ajouté par Kassem et Humza
                #Créer un vaisseau seulement si une station existe
                #################################
        #        for j in i.planetes:
        #            if j.station :
                #################################
        #                print("Création d'un vaisseau d'attaque")
                        #v=Vaisseau(self.nom,i,20,20)
        #                v=VaisseauAttaque(self.nom,i)
        #                self.vaisseauxinterstellaires.append(v)
        #                return 1

    def creervaisseaubombarde(self,id):
        for i in self.systemesvisites:
            if i.id==id:
                #Code ajouté par Kassem et Humza
                #Créer un vaisseau seulement si une station existe
                #################################
                for j in i.planetes:
                    if j.station :
                #################################
                        print("Création d'un vaisseau bombardier")
                        #v=Vaisseau(self.nom,i,20,20)
                        v=VaisseauBombardier(self.nom,i)
                        self.vaisseauxinterstellaires.append(v)
                        return 1

    def creervaisseaucargo(self,id):
        for i in self.systemesvisites:
            if i.id==id:
                #Code ajouté par Kassem et Humza
                #Créer un vaisseau seulement si une station existe
                #################################
                for j in i.planetes:
                    if j.station :
                #################################
                        print("Création d'un cargo")
                        #v=Vaisseau(self.nom,i,20,20)
                        v=VaisseauCargo(self.nom,i)
                        self.vaisseauxinterstellaires.append(v)
                        return 1

    def creervaisseautransporteur(self,id):
        for i in self.systemesvisites:
            if i.id==id:
                #Code ajouté par Kassem et Humza
                #Créer un vaisseau seulement si une station existe
                #################################
                for j in i.planetes:
                    if j.station :
                #################################
                        print("Création d'un transporteur")
                        #v=Vaisseau(self.nom,i,20,20)
                        v=VaisseauTransporteur(self.nom,i)
                        self.vaisseauxinterstellaires.append(v)
                        return 1

    def creervaisseautransporteur(self,id):
        i = self.systemesvisites[id]
        #Créer un vaisseau seulement si une station existe
        for j in i.planetes:
            if i.planetes[j].station :
                #v=Vaisseau(self.nom,i,20,20)
                v=VaisseauTransporteur(self.nom,i)
                self.vaisseauxinterstellaires[v.id] = v
                return 1
##-lau--------------------------------------------------------------------------------------^
    def ciblerdestination(self,ids):
        idori,iddesti=ids
        # Branche mig
        #vorigine=self.vaisseauxinterstellaires[idori]
        #sdesti=self.parent.systemes[iddesti]
        #vorigine.ciblerdestination(sdesti)
        for i in self.vaisseauxinterstellaires:
            if i.id== idori:
                for j in self.parent.systemes:
                    if j.id== iddesti:
                        #i.cible=j
                        i.ciblerdestination(j)
                        return
                for j in self.systemesvisites:
                    if j.id== iddesti:
                        #i.cible=j
                        i.ciblerdestination(j)
                        return

    def ciblerdestinationsysteme(self,ids,x):
        idori,iddesti=ids
        for i in self.vaisseauxinterstellaires:
            if i.id== idori:
                i.ciblerdestinationsysteme(x)
                """
                for j in self.parent.systemes:
                    if j.id== iddesti:
                        #i.cible=j
                        i.ciblerdestinationsysteme(j)
                        return
                for j in self.systemesvisites:
                    if j.id== iddesti:
                        #i.cible=j
                        i.ciblerdestinationsysteme(j)
                        return
                """

    def prochaineaction(self): # NOTE : cette fonction sera au coeur de votre developpement
        vicles=list(self.vaisseauxinterstellaires.keys())
        vicles.sort()
        for i in vicles:
            print("")
            if self.vaisseauxinterstellaires[i].cible:
                rep=self.vaisseauxinterstellaires[i].avancer()
                if rep:
                    if rep.proprietaire=="inconnu":
                        if rep.id not in self.systemesvisites.keys():
                            self.systemesvisites[rep.id]=rep
                            self.parent.changerproprietaire(self.nom,self.couleur,rep)
            # Touched
            if self.vaisseauxinterstellaires[i].ciblesysteme:
                rep=i.avancersysteme()
                
        