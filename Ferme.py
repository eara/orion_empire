# -*- encoding: utf-8 -*-

from Id import Id

class Ferme():
    def __init__(self,parent,nom,systemeid,planeteid,x,y):   

        self.id=Id.prochainid()
        self.parent=parent
        self.x=x
        self.y=y
        self.systemeid=systemeid
        self.planeteid=planeteid
        self.entrepot=0
        self.coupwood = 400
        self.coupmine = 250
        self.coupunite = 4
        self.dureconstruction = 20