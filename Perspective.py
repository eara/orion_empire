# -*- encoding: utf-8 -*-

from tkinter import *
from PIL import Image,ImageDraw, ImageTk

class Perspective(Frame):
    def __init__(self,parent):
        Frame.__init__(self,parent.cadrejeu)
        self.parent=parent
        self.modele=None
        self.cadreetatactif=None
        self.cadreetatactifplanete=None
        self.cadreetatactifbatiment = None
        self.images={}

        self.selected = ()
        self.drag_data = {"x": None, "y": None, "item": None}
        self.maselection = ()
        self.selectionMultiple = False
        self.cadrevue=Frame(self,width=400,height=400, bg="lightgreen")
        self.cadrevue.pack(side=LEFT,expand=1,fill=BOTH)

        self.cadreinfo=Frame(self,width=200,height=200,bg="darkgrey")
        self.cadreinfo.pack(side=LEFT,fill=Y)
        self.cadreinfo.pack_propagate(0)
        self.cadreetat=Frame(self.cadreinfo,width=200,height=200,bg="grey20")
        self.cadreetat.pack()

        self.scrollX=Scrollbar(self.cadrevue,orient=HORIZONTAL)
        self.scrollY=Scrollbar(self.cadrevue)
        self.canevas=Canvas(self.cadrevue,width=800,height=800,bg="grey11",
                             xscrollcommand=self.scrollX.set,
                             yscrollcommand=self.scrollY.set)

        #self.canevas.bind("<Button>",self.cliquervue)

        self.scrollX.config(command=self.canevas.xview)
        self.scrollY.config(command=self.canevas.yview)
        self.canevas.grid(column=0,row=0,sticky=N+E+W+S)
        self.cadrevue.columnconfigure(0,weight=1)
        self.cadrevue.rowconfigure(0,weight=1)
        self.scrollX.grid(column=0,row=1,sticky=E+W)
        self.scrollY.grid(column=1,row=0,sticky=N+S)

        self.labid=Label(self.cadreinfo,text=self.parent.nom)
        self.labid.pack()

        self.cadreetataction=Frame(self.cadreetat,width=200,height=200,bg="grey20")

        self.cadreetatmsg=Frame(self.cadreetat,width=200,height=200,bg="grey20")

        self.cadreminimap=Frame(self.cadreinfo,width=200,height=200,bg="grey20")
        self.cadreminimap.pack(side=BOTTOM)
        self.minimap=Canvas(self.cadreminimap,width=200,height=200,bg="grey11")
        self.minimap.bind("<Button>",self.cliquerminimap)
        self.minimap.pack()

        # Bindings
        self.canevas.bind('<Button-1>', self.onLeftClick)
        self.canevas.bind('<B1-Motion>', self.onDrag)
        self.canevas.bind('<ButtonRelease-1>', self.onRealease)
        self.canevas.bind( '<ButtonRelease-3>', self.onRightClik)
        self.canevas.bind('<Double-Button-1>', self.doubleClick)
        self.canevas.bind('<Triple-Button-1>', self.changePerspective)

        self.cadreetatactionplanete=Frame(self.cadreetat,width=200,height=200,bg="grey20")
        self.cadreetatactionbatiment=Frame(self.cadreetat,width=200,height=200,bg="grey20")

    def changePerspective(self, event):
        if self.parent.modecourant.__class__.__name__ == "VuePlanete":
            self.voirsysteme()
        elif self.parent.modecourant.__class__.__name__ == "VueSysteme":
            self.voirgalaxie()
        
    def drawSelection(self):
        self.canevas.delete("marqueur")
        pad = 10
        for i in self.selected:
            bbox = self.canevas.bbox((i,))
            self.canevas.create_oval( bbox[0] - pad, bbox[1] - pad, bbox[2] + pad , bbox[3] + pad , dash=(25,25), outline = "blue", tags=("marqueur",))

    def unselect(self):
        self.maselection = ()
        self.selected = ()
        self.canevas.delete("marqueur")

    def onLeftClick(self, event):
        self.drag_data['item'] = self.canevas.gettags(CURRENT)
        self.drag_data['x'] = self.canevas.canvasx(event.x)
        self.drag_data['y'] = self.canevas.canvasy(event.y)
        print(self.drag_data['x'], self.drag_data['y'], event.x, event.y, self.drag_data['item'])

    def onDrag(self, event):
        if ('selsqr',) == self.canevas.gettags("selsqr"):
            self.canevas.delete("selsqr")
        self.canevas.create_rectangle(self.drag_data['x'], self.drag_data['y'], self.canevas.canvasx(event.x), self.canevas.canvasy(event.y), fill = "blue", stipple = "gray12", tags = ('selsqr',))
        self.canevas.update()
        
    def doubleClick(self, event):
        print("CHU DANS DOUBLE CLICK !!!!")
        ##
        tup = self.canevas.find_overlapping(self.drag_data['x'], self.drag_data['y'], self.canevas.canvasx(event.x), self.canevas.canvasy(event.y))

        if "systeme" in self.maselection:
            self.voirsysteme()   
        
        if "planete" in self.maselection:
            self.voirplanete()   
                
    def voirsysteme(self):
        pass
    
    def voirplanete(self):
        pass
    
    def voirgalaxie(self):
        pass

    def onRealease(self, event):
        # Tout déselectionner avant de refaire une nouvelle selection
        self.unselect()
        self.canevas.delete("selsqr") # Carré de sélection
        ## Logique JMD
        print("Region inconnue")
        self.maselection=None
        self.changecadreetatplanete(None)
        self.lbselectecible.pack_forget()
        self.canevas.delete("selecteur")
        ##
        tup = self.canevas.find_overlapping(self.drag_data['x'], self.drag_data['y'], self.canevas.canvasx(event.x), self.canevas.canvasy(event.y))
        print("On release tup",tup)
        print("PRE TRAITEMENT: maselection = ", self.maselection, "selected = ", self.selected)
        # Quand on est la galaxie
        if self.parent.modecourant.__class__.__name__ == "VueGalaxie":
            if len(tup) == 2:
                #if 'systeme' in self.canevas.gettags(tup[1]):
                self.maselection = self.canevas.gettags(tup[1])
                self.selected = (tup[1],)
                self.montresystemeselection()
                print(tup)
        elif self.parent.modecourant.__class__.__name__ == "VueSysteme":
            if len(tup) == 2:
                #if 'systeme' in self.canevas.gettags(tup[1]):
                self.maselection = self.canevas.gettags(tup[1])
                self.selected = (tup[1],)
                self.montresystemeselection()
                print(tup)
        else:
            if len(tup) > 1:
                # Prendre seulement les vaisseau
                for element in tup:
                    if 'unite' in self.canevas.gettags(element):
                        self.selected += (element,)
                        self.maselection = ()
                        self.maselection = self.maselection + self.canevas.gettags(element)
            else:
                self.maselection = self.canevas.gettags(tup)
                self.selected = tup
                if self.parent.contructObjet:
                    print(self.parent.contructObjet)
                    self.parent.callBack['construire']((self.canevas.canvasx(event.x), self.canevas.canvasy(event.y)))

        #self.montresystemeselection() # Fonction pour passer à une autre Perspective
        """
        elif len(tup) == 1 and self.parent.modecourant != self.parent.modes["galaxie"]:
            self.maselection = self.canevas.gettags(tup)
            self.selected = tup
            #self.montresystemeselection()
            #self.showSelectionInfo()

        ##-------------------------------------------------------matlau
        elif len(tup) == 0 and self.parent.modecourant != self.parent.modes["galaxie"] and self.macommande != None:

            if self.macommande == "mine":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creermine(self.parent.nom,self.systemeid,self.planeteid,x,y)
                self.macommande=None
            elif self.macommande == "baraque":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerbaraque(self.parent.nom,self.systemeid,self.planeteid,x,y)
                self.macommande=None
<<<<<<< HEAD
            elif self.macommande == "unite":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerunite(self.parent.nom,self.systemeid,self.planeteid,x,y)
                self.macommande=None

=======
            elif self.macommande == "labo":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerlabo(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "ferme":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerferme(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "wood":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerwood(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "church":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerchurch(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "labo":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerlabo(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "ferme":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerferme(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "wood":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerwood(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            elif self.macommande == "church":
                x=self.canevas.canvasx(event.x)
                y=self.canevas.canvasy(event.y)
                self.parent.parent.creerchurch(self.parent.nom,self.systemeid,self.planeteid,x,y)         
                self.macommande=None
            
>>>>>>> joel
            self.maselection=None


        elif len(tup) == 0 and self.parent.modecourant != self.parent.modes["galaxie"]:
            print("Region inconnue")
            print(self.macommande)
            self.maselection=None
            self.changecadreetatplanete(None)
            self.lbselectecible.pack_forget()
            self.canevas.delete("selecteur")
        ##-------------------------------------------------------matlau
        """
        print("POST TRAITEMENT: maselection = ", self.maselection, "selected = ", self.selected)


    def onRightClik(self, event):
        # Détermine sur quels éléments le joueur à cliquer
        self.parent.contructObjet = None
        if "infrastructure" not in self.canevas.gettags( self.selected ):
            objetModele = self.parent.parent.getModelObjet()
            for obj in objetModele:
                obj.newX = self.canevas.canvasx(event.x)
                obj.newY = self.canevas.canvasy(event.y)
            
                
        
        
        #tup = self.canevas.gettags(CURRENT)
        #if 'planet' in tup: # Sur une plannet
        #    print('Planet')
        #elif 'vaisseau' in tup: # sur un vesseau
        #    print('vaisseau')
        #else:
            #self.c.setPosition((event.x, event.y), self.selected)
        #    self.callBack['appendAction']("setPosition" ,[(event.x, event.y), self.selected])
            #self.c.moving()

    def showSelectionInfo(self):
        print("MES SUPER SELECTION: ",self.maselection)

    #def cliquervue(self,evt):
    #    print("LE MONDE EST A PLEURER")

    #def cliquerminimap(self,evt):
    #    pass

    def changecadreetat(self,cadre):
        if self.cadreetatactif:
            self.cadreetatactif.pack_forget()
            self.cadreetatactif=None
        if cadre:
            self.cadreetatactif=cadre
            self.cadreetatactif.pack()
    ##------------------------------------matlau
    def changecadreetatplanete(self,cadre):
        if self.cadreetatactifplanete:
            self.cadreetatactifplanete.pack_forget()
            self.cadreetatactifplanete=None
        if cadre:
            self.cadreetatactifplanete=cadre
            self.cadreetatactifplanete.pack()

    def changecadreetatbatiment(self,cadre):
        if self.cadreetatactifbatiment:
            self.cadreetatactifbatiment.pack_forget()
            self.cadreetatactifbatiment=None
        if cadre:
            self.cadreetatactifbatiment=cadre
            self.cadreetatactifbatiment.pack()
            
    def animationDeplacement(self, id, deltaX, deltaY):
        self.canevas.move(self.canevas.find_withtag(id), deltaX, deltaY)

    ##-------------------------------------matlau